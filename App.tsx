/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {} from 'react-native';

import Router from './src/router';
import navigationService from './src/services/navigationService';
import ble from './src/services/ble';
import {cloneResourceFromAssetToDocument} from './src/screens/Dfu';

console.disableYellowBox = true;

type Props = {};
export default class App extends Component<Props> {
  componentDidMount() {
    ble.init();
    cloneResourceFromAssetToDocument();
  }

  componentWillUnmount() {
    ble.destroyBle();
  }

  render() {
    return (
      <Router
        ref={navigatorRef => {
          navigationService.setTopLevelNavigator(navigatorRef);
        }}
        onNavigationStateChange={navigationService.onNavigationStateChange}
      />
    );
  }
}
