import {
  createSwitchNavigator,
  createAppContainer,
  SafeAreaView,
} from 'react-navigation';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import React from 'react';
import {View, ToastAndroid, BackHandler, StatusBar} from 'react-native';

import LoadingDataScreen from '../screens/LoadingDataScreen';

import ScanningScreen from '../screens/ScanningScreen';
import SelectType from '../screens/SelectDevice/SelectType';
import DemiScreen from '../screens/DemiScreen';
import EffectScreen from '../screens/EffectScreen';
import TurnScreen from '../screens/TurnScreen';
import BreakScreen from '../screens/BreakScreen';
import HazardScreen from '../screens/HazardScreen';
import BLeStartScreen from '../screens/BLeStartScreen';
import BLeHazardScreen from '../screens/BLeHazardScreen';
import BLeBreakScreen from '../screens/BLeBreakScreen';
import BLeTurnScreen from '../screens/BLeTurnScreen';
import ScreenLoading, {
  setRef as setRefScreenLoading,
  showScreenLoading,
} from '../components/ScreenLoading';
import TabBarComponent from '../components/TabBarComponent';
import TabBarBLe from '../components/TabBarBLe';
import ble from '../services/ble';
import Popup, {setRef, responsePopup} from '../components/Popup';
import navigationService from '../services/navigationService';
import Images from '../assets/Images';
import eventBus from '../services/eventBus';
import {
  EVENT_BUS,
  BLE_RESPONSE_DATA_TYPE,
  LED_MODE,
  STOP_F1_BLE,
} from '../constants';
import BackHandlerHelper from '../services/BackHandlerHelper';
import colors from '../theme/colors';
import SelectDevice from '../screens/SelectDevice';
import Dfu from '../screens/Dfu';

const AppTab = createBottomTabNavigator(
  {
    DemiScreen: {
      screen: DemiScreen,
    },
    EffectScreen: {
      screen: EffectScreen,
    },
    TurnScreen: {
      screen: TurnScreen,
    },
    BreakScreen: {
      screen: BreakScreen,
    },
    HazardScreen: {
      screen: HazardScreen,
    },
  },
  {
    // lazy: false
    tabBarComponent: TabBarComponent,
  },
);
const BLeTab = createBottomTabNavigator(
  {
    BLeStartScreen: {
      screen: BLeStartScreen,
    },
    BLeTurnScreen: {
      screen: BLeTurnScreen,
    },
    BLeBreakScreen: {
      screen: BLeBreakScreen,
    },
    BLeHazardScreen: {
      screen: BLeHazardScreen,
    },
  },
  {
    // lazy: false
    tabBarComponent: TabBarBLe,
  },
);

class TabWrapper extends React.PureComponent {
  static router = AppTab.router;

  constructor() {
    super();
  }

  componentDidMount() {
    ble.addDisconnectCallback(this.onBleDisconnect);
    eventBus.addListener(
      EVENT_BUS.CHANGE_MODE_FROM_HARDWARE,
      this.onChangeModeFromHardware,
    );
    setTimeout(() => StatusBar.setBarStyle('light-content'), 100);
  }

  componentWillUnmount() {
    eventBus.removeListener(this.onChangeModeFromHardware);
  }

  onChangeModeFromHardware = currentModeData => {
    const {mode, effect, color, speed} = ble.extractDataFromResponse(
      currentModeData,
      BLE_RESPONSE_DATA_TYPE.CURRENT_MODE,
    );
    const {navigation} = this.props
    const specificDevice = navigation.getParam('specificDevice', '');

    switch (mode) {
      case LED_MODE.DEMI:
        if (specificDevice === 'F1_BLE') {
          navigation.navigate('AppBLe');
        } else {
          navigationService.navigate('DemiScreen', {
            currentModeData: {mode, effect, color, speed},
          });
        }
        break;
      case LED_MODE.EFFECT:
        if (specificDevice === 'F1_BLE') {
          navigation.navigate('AppBLe');
        } else {
          navigationService.navigate('EffectScreen', {
            currentModeData: {mode, effect, color, speed},
          });
        }
        break;
      case LED_MODE.BREAK:
        if (specificDevice === 'F1_BLE') {
          navigation.navigate('AppBLe');
        } else {
          navigationService.navigate('BreakScreen', {
            currentModeData: {mode, effect, color, speed},
          });
        }
       
        break;

      case LED_MODE.TURN_INDICATOR:
        if (specificDevice === 'F1_BLE') {
          navigation.navigate('AppBLe');
        } else {
          navigationService.navigate('TurnScreen', {
            currentModeData: {mode, effect, color, speed},
          });
        }
       
        break;

      case LED_MODE.HAZARD:
        if (specificDevice === 'F1_BLE') {
          navigation.navigate('AppBLe');
        } else {
          navigationService.navigate('HazardScreen', {
            currentModeData: {mode, effect, color, speed},
          });
        }
       
        break;

      default:
        break;
    }
  };

  onBleDisconnect = () => {
    showScreenLoading(false);
    responsePopup({
      title: 'Kết nối gián đoạn',
      subtitle: '',
      retryCb: () => navigationService.navigate('ScanningScreen'),
      touchOutsideToDismiss: false,
      icon: Images.brokenLink,
    });
  };

  onBackPress = () => {
    if (!this.backPressed) {
      this.backPressed = true;
      ToastAndroid.show(
        'Ấn back một lần nữa để thoát ứng dụng.',
        ToastAndroid.SHORT,
      );
      setTimeout(() => {
        this.backPressed = false;
      }, 2000);
    } else {
      BackHandler.exitApp();
    }
  };

  render() {
    const {navigation} = this.props;
    return (
      <View style={{flex: 1, backgroundColor: colors.mainDark}}>
        <SafeAreaView style={{flex: 1, backgroundColor: colors.secondaryDark}}>
          <View
            style={{
              width: '100%',
              height: 100,
              position: 'absolute',
              bottom: 0,
              left: 0,
              backgroundColor: colors.mainDark,
            }}
          />
          <View style={{flex: 1, backgroundColor: colors.mainDark}}>
            <AppTab navigation={navigation} />
          </View>
        </SafeAreaView>
        <Popup ref={_ref => setRef(_ref)} />
        <ScreenLoading ref={_ref => setRefScreenLoading(_ref)} />
        <BackHandlerHelper
          screenName="TabWrapper"
          onBackPress={this.onBackPress}
        />
      </View>
    );
  }
}

class BLeTabWrapper extends React.PureComponent {
  static router = BLeTab.router;

  constructor() {
    super();
  }

  componentDidMount() {
    ble.addDisconnectCallback(this.onBleDisconnect);
    eventBus.addListener(
      EVENT_BUS.CHANGE_MODE_FROM_HARDWARE,
      this.onChangeModeFromHardware,
    );
    setTimeout(() => StatusBar.setBarStyle('light-content'), 100);
  }

  componentWillUnmount() {
    eventBus.removeListener(this.onChangeModeFromHardware);
  }

  onChangeModeFromHardware = currentModeData => {
    const {mode, effect, color, speed} = ble.extractDataFromResponse(
      currentModeData,
      BLE_RESPONSE_DATA_TYPE.CURRENT_MODE,
    );
    const {navigation} = this.props
    const specificDevice = navigation.getParam('specificDevice', '');

    switch (mode) {
      case LED_MODE.DEMI:
        if (specificDevice === 'F1_BLE') {
          navigation.navigate('AppBLe');
        } else {
          // navigationService.navigate('DemiScreen', {
          //   currentModeData: {mode, effect, color, speed},
          // });
        }
        break;
      case LED_MODE.EFFECT:
        if (specificDevice === 'F1_BLE') {
          navigation.navigate('AppBLe');
        } else {
          // navigationService.navigate('EffectScreen', {
          //   currentModeData: {mode, effect, color, speed},
          // });
        }
        break;
      case LED_MODE.BREAK:
        if (specificDevice === 'F1_BLE') {
          navigation.navigate('AppBLe');
        } else {
          // navigationService.navigate('BreakScreen', {
          //   currentModeData: {mode, effect, color, speed},
          // });
        }
       
        break;

      case LED_MODE.TURN_INDICATOR:
        if (specificDevice === 'F1_BLE') {
          navigation.navigate('AppBLe');
        } else {
          // navigationService.navigate('TurnScreen', {
          //   currentModeData: {mode, effect, color, speed},
          // });
        }
       
        break;

      case LED_MODE.HAZARD:
        if (specificDevice === 'F1_BLE') {
          navigation.navigate('AppBLe');
        } else {
          // navigationService.navigate('HazardScreen', {
          //   currentModeData: {mode, effect, color, speed},
          // });
        }
       
        break;

      default:
        break;
    }
  };

  onBleDisconnect = () => {
    showScreenLoading(false);
    responsePopup({
      title: 'Kết nối gián đoạn',
      subtitle: '',
      retryCb: () => navigationService.navigate('ScanningScreen'),
      touchOutsideToDismiss: false,
      icon: Images.brokenLink,
    });
  };

  onBackPress = () => {
    if (!this.backPressed) {
      this.backPressed = true;
      ToastAndroid.show(
        'Ấn back một lần nữa để thoát ứng dụng.',
        ToastAndroid.SHORT,
      );
      setTimeout(() => {
        this.backPressed = false;
      }, 2000);
    } else {
      BackHandler.exitApp();
    }
  };

  render() {
    const {navigation} = this.props;
    return (
      <View style={{flex: 1, backgroundColor: colors.mainDark}}>
        <SafeAreaView style={{flex: 1, backgroundColor: colors.secondaryDark}}>
          <View
            style={{
              width: '100%',
              height: 100,
              position: 'absolute',
              bottom: 0,
              left: 0,
              backgroundColor: colors.mainDark,
            }}
          />
          <View style={{flex: 1, backgroundColor: colors.mainDark}}>
            <BLeTab navigation={navigation} />
          </View>
        </SafeAreaView>
        <Popup ref={_ref => setRef(_ref)} />
        <ScreenLoading ref={_ref => setRefScreenLoading(_ref)} />
        <BackHandlerHelper
          screenName="BLeTabWrapper"
          onBackPress={this.onBackPress}
        />
      </View>
    );
  }
}

const routeSwitch = createSwitchNavigator({
  LoadingDataScreen: {screen: LoadingDataScreen},
  SelectDevice: {screen: SelectDevice},
  SelectType: {screen: SelectType},
  Dfu: {screen: Dfu},
  ScanningScreen: {screen: ScanningScreen},
  AppBLe: {screen: BLeTabWrapper},
  App: {screen: TabWrapper},
});

export default createAppContainer(routeSwitch);
