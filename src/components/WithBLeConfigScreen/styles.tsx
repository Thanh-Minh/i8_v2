import {StyleSheet} from 'react-native';

const MARGIN_HORIZONTAL = 10;

export default StyleSheet.create({
  screenTitle: {
    alignSelf: 'center',
    padding: 10,
    fontSize: 18,
  },
  colorPicker: {
    marginTop: 10,
    marginHorizontal: MARGIN_HORIZONTAL,
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  colorIndicatorWrapper: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  circlePicked: {
    backgroundColor: '#EEEEEE',
    borderWidth: 1,
    borderColor: '#707070',
    elevation: 1,
    shadowColor: 'rgb(46, 48, 58)',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.3,
    shadowRadius: 2,
  },
  title: {
    paddingBottom: 10,
  },
  configWrapper: {
    marginTop: 20,
  },
  groupColor1: {
    marginHorizontal: 30,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  groupColor2: {
    marginTop: 20,
    marginHorizontal: 30,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  modeList: {
    marginHorizontal: MARGIN_HORIZONTAL,
  },
  boxFrame:{
    marginVertical: 20,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 10,
  },
  backgroundFrame:{
    height: 200,
    zIndex:9
  },
  container:{
    alignItems: 'center',
  },
  boxContent:{
    flexDirection: 'row',
    zIndex: 10,
    position: 'absolute',
    top: 100,
  }
});
