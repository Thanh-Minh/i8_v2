/**
 * @flow
 */
import React from 'react';
import {TouchableOpacity, Image} from 'react-native';

const RecommendColor = ({
  image,
  colorDisplay,
  colorReal,
  onPress,
  style,
  disabled,
}) => (
  <TouchableOpacity
    hitSlop={{top: 5, left: 5, right: 5, bottom: 5}}
    disabled={disabled}
    style={[
      {
        // width: 44,
        // height: 44,
        // backgroundColor: colorDisplay,
        // borderRadius: 44,
      },
      style,
    ]}
    onPress={() => onPress(colorReal)}>
    <Image
      source={image}
      style={{
        width: 44,
        height: 44,
        resizeMode: 'contain',
      }}
    />
  </TouchableOpacity>
);

export default RecommendColor;
