import React from 'react';
import {
  Text,
  View,
  ImageBackground,
  Alert,
  TouchableOpacity,
} from 'react-native';
import styles from './styles';
import Images from '../../assets/Images';
import ble from '../../services/ble';
import { I8_BLE } from '../../constants';
import navigationService from '../../services/navigationService';

type Props = { title: string; rightButton: boolean; navigation: object };
type States = {};

class ScreenHeader extends React.PureComponent<Props, States> {
  constructor(props) {
    super(props);

  }
  static defaultProps = {
    rightButton: true,
  };



  render() {
    const { title, rightButton, navigation } = this.props;
    return (
      <View style={styles.rootContainer}>
        <ImageBackground
          imageStyle={styles.icon}
          style={styles.icon}
          source={Images.headerIcon}
        />
        <Text
          style={styles.title}>{title}</Text>
        <TouchableOpacity
          disabled={!rightButton}
          style={{ opacity: rightButton ? 1 : 0 }}
          hitSlop={{ top: 15, left: 15, right: 15, bottom: 15 }}
          onPress={() => {
            if (title == 'WINNER X' || 'VAIRO') {
              console.log("GO BACK--------")
              navigationService.navigate('SelectDevice');
            }
            else {
              Alert.alert('Ngắt kết nối thiết bị', undefined, [
                {
                  text: 'Đồng ý',
                  style: 'destructive',
                  onPress: () => {
                    ble.addDisconnectCallback(null);
                    ble.destroyBle();
                    navigationService.navigate('SelectDevice');
                  },
                },
                { text: 'Huỷ', style: 'cancel' },
              ]);
            }
          }}>
          <ImageBackground
            imageStyle={styles.iconQuit}
            style={styles.quit}
            source={Images.iconQuit}
          />
        </TouchableOpacity>
      </View>
    );
  }
}

export default ScreenHeader;
