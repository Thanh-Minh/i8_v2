import {StyleSheet} from 'react-native';

import colors from '../../theme/colors';

export default StyleSheet.create({
  rootContainer: {
    flexDirection: 'row',
    backgroundColor: colors.secondaryDark,
    paddingHorizontal: 10,
    paddingVertical: 7,
    alignItems: 'center',
  },
  title: {
    fontSize: 25,
    fontWeight: 'bold',
    color: 'white',
    flex: 1,
    textAlign: 'center',
    alignSelf: 'center',
  },
  icon: {
    height: 40,
    width: 40,
  },

  quit: {
    height: 30,
    width: 30,
  },

  iconQuit: {
    tintColor: 'white',
  },
});
