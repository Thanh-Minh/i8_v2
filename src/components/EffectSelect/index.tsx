/**
 * @flow
 */
import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ImageBackground,
  Image,
} from 'react-native';
import colors from '../../theme/colors';
import Images from '../../assets/Images';

type Props = {};
type States = {};

class Component extends React.PureComponent<Props, States> {
  constructor(props: any) {
    super(props);
  }

  render() {
    const outerR = 55;

    const {effect, selectedEffect, name, onChangeEffect, index} = this.props;
    return (
      <TouchableOpacity
        hitSlop={{top: 10, right: 10, left: 10, bottom: 10}}
        style={
          {
            // borderWidth: 1,
            // borderColor: '#707070',
            // justifyContent: 'center',
            // alignItems: 'center',
            // width: outerR,
            // height: outerR,
            // borderRadius: outerR,
            // backgroundColor:
            //   effect === selectedEffect ? 'white' : colors.mainLight,
          }
        }
        onPress={() => onChangeEffect(effect, index)}>
        <ImageBackground
          source={
            effect === selectedEffect
              ? Images.selectEffectBgActive
              : Images.selectEffectBg
          }
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            width: outerR,
            height: outerR,
          }}
          imageStyle={{
            width: outerR,
            height: outerR,
          }}
          resizeMode="contain">
          <Text
            style={{
              fontWeight: 'bold',
              fontSize: 27,
              // color: effect === selectedEffect ? colors.mainLight : 'white',
              color: 'white',
              textAlign: 'center',
              textAlignVertical: 'center',
              marginBottom: 5,
            }}>
            {name}
          </Text>
        </ImageBackground>
      </TouchableOpacity>
    );
  }
}

export default Component;
