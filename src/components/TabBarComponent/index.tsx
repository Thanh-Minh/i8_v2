/**
 * @flow
 */
import React from 'react';
import { View, TouchableOpacity, StyleSheet, Text, Image } from 'react-native';
import { TabBarBottomProps, NavigationRoute } from 'react-navigation';
import colors from '../../theme/colors';
import Images from '../../assets/Images';
import ble from '../../services/ble';
import { I8_BLE, WINNER_HAU } from '../../constants';

// just for ide hints
interface TabBarProps extends TabBarBottomProps { }

interface TabBarState { }

class Component extends React.PureComponent<TabBarProps, TabBarState> {
  navigationStateIndex = null;

  renderTabBarButton(route: NavigationRoute, idx: any) {
    const { navigation, getLabelText, renderIcon } = this.props;
    const currentIndex = navigation.state.index;
    const isForced = currentIndex === idx;
    const color = isForced ? colors.mainLight : 'white';

    const label = getLabelText({
      route,
      focused: isForced,
      index: idx,
    });

    let tabIcon = null;
    switch (route.routeName) {
      case 'DemiScreen':
        tabIcon = Images.iconTabDemi;
        break;
      case 'EffectScreen':
        tabIcon = Images.iconTabEffect;
        break;
      case 'TurnScreen':
        tabIcon = Images.iconTabIndicator;
        break;
      case 'BreakScreen':
        tabIcon = Images.iconTabBreak;
        break;
      case 'HazardScreen':
        tabIcon = Images.iconTabHazard;
        break;
      default:
        break;
    }
    // Add hardzard tab vs I8X
    /*
    if (route.routeName === 'HazardScreen' && ble.getDeviceName() === WINNER_HAU) {
      return null;
    }*/

    return (
      <TouchableOpacity
        onPress={() => {
          if (currentIndex != idx) {
            navigation.navigate(route.routeName);
          }
        }}
        style={[
          styles.tabBarUnit,
          // {
          //   backgroundColor: isForced ? colors.mainDark : colors.mainLight,
          // },
        ]}
        key={route.routeName}>
        <Image source={tabIcon} style={styles.tabIcon} />
        <Image
          source={currentIndex === idx ? Images.tabActive : Images.tabInactive}
          style={styles.isActive}
        />
        {/* {renderIcon({
          route,
          tintColor: color,
          focused: currentIndex === idx,
          index: idx,
        })} */}
        {/* <Text style={[styles.label, {color}]}>{label}</Text> */}
      </TouchableOpacity>
    );
  }

  render() {
    const { navigation, style } = this.props;
    const tabBarButtons = navigation.state.routes.map(
      this.renderTabBarButton.bind(this),
    );
    return <View style={[style, styles.tabBar]}>{tabBarButtons}</View>;
  }
}

export default Component;

const styles = StyleSheet.create({
  tabBar: {
    flexDirection: 'row',
  },
  tabBarUnit: {
    flex: 1,
    marginBottom: 2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.mainDark,
  },
  label: {
    marginLeft: 5,
    fontSize: 14,
    fontWeight: 'bold',
  },

  tabIcon: {
    width: '90%',
    height: 61,
    resizeMode: 'contain',
    zIndex: 1,
  },
  isActive: {
    height: 17,
    width: '100%',
    resizeMode: 'contain',
    marginTop: -17 / 2,
  },
});
