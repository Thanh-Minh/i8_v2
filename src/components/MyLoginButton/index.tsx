/**
 * @flow
 */
import React from 'react';
import {TouchableOpacity, Text} from 'react-native';

const MyLoginButton = ({
  title,
  onPress,
  color,
}: {
  title: string;
  onPress: Function;
  color: string;
}) => (
  <TouchableOpacity
    style={{
      backgroundColor: color,
      height: 41,
      width: 310,
      marginVertical: 6,
      borderRadius: 41,
      justifyContent: 'center',
      alignItems: 'center',
    }}
    onPress={onPress}>
    <Text
      style={{
        fontSize: 16,
        color: 'white',
      }}>
      {title}
    </Text>
  </TouchableOpacity>
);

export default MyLoginButton;
