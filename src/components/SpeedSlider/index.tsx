/**
 * @flow
 */
import React from 'react';
// import Slider from 'react-native-slider';
import Slider from '../BaseSlider';
// import Slider from '@react-native-community/slider';

import Images from '../../assets/Images';
import colors from '../../theme/colors';
import {Platform} from 'react-native';

const MAX_VALUE = 250;
const MIN_VALUE = 50;
const STEP = 5;

type Props = {
  onSlidingComplete: (value: number) => void;
  disabled: boolean;
  style?: Record<string, any>;
  value: number;
  onSlidingStart: (value: number) => void;
  maxValue?: number;
  minValue?: number;
};
type States = {};

class Component extends React.PureComponent<Props, States> {
  static defaultProps = {
    style: {},
    maxValue: MAX_VALUE,
    minValue: MIN_VALUE,
  };

  constructor(props: any) {
    super(props);

    const {value, onSlidingComplete, minValue, maxValue} = props;
    if (value < minValue) {
      onSlidingComplete(minValue);
    } else if (value > maxValue) {
      onSlidingComplete(maxValue);
    }
  }

  componentWillReceiveProps(nextProps: Props) {
    const {value, onSlidingComplete, minValue, maxValue} = this.props;
    if (nextProps.value !== value) {
      if (nextProps.value < minValue) {
        onSlidingComplete(minValue);
      } else if (nextProps.value > maxValue) {
        onSlidingComplete(maxValue);
      }
    }
  }

  componentWidth = 0;

  render() {
    const {
      onSlidingComplete,
      disabled,
      style,
      value,
      onSlidingStart,
      maxValue,
      minValue,
    } = this.props;
    return (
      <Slider
        onLayout={e => {
          this.componentWidth = e.nativeEvent.layout.width;
        }}
        onResponderGrant={e => {
          if (Platform.OS === 'ios') {
            const newValue =
              (e.nativeEvent.locationX / this.componentWidth) *
              (maxValue - minValue);
            const realValue = maxValue - newValue;
            onSlidingComplete(realValue);
          }
        }}
        onSlidingStart={onSlidingStart}
        hitSlop={{left: 15, top: 15, right: 15, bottom: 15}}
        value={maxValue - value}
        style={[
          {
            // marginHorizontal: 46,
            marginVertical: Platform.select({android: 20, ios: 0}),
          },
          style,
        ]}
        disabled={disabled}
        onSlidingComplete={newValue => {
          const realValue = maxValue - newValue;
          onSlidingComplete(realValue);
        }}
        trackStyle={{height: 11, borderRadius: 11}}
        minimumValue={minValue - minValue}
        maximumValue={maxValue - minValue}
        step={STEP}
        minimumTrackTintColor="white"
        maximumTrackTintColor="#7D7E81"
        thumbImage={Images.sliderThumb}
        thumbTouchSize={{width: 37, height: 37}}
        thumbStyle={{width: 37, height: 37}}
        
      />
    );
  }
}

export default Component;
