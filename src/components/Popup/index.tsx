import React from 'react';
import {BackHandler, Platform, View, Text, ImageBackground} from 'react-native';
import {Dialog, DialogContent, SlideAnimation} from 'react-native-popup-dialog';

import Button from '../Button';
import styles from './Popup.style';
import Images from '../../assets/Images';
import navigationService from '../../services/navigationService';

class Component extends React.PureComponent {
  constructor(props: any) {
    super(props);
    this.state = {
      isShow: false,
      // content
      title: '',
      subtitle: '',
      retryCb: () => false,
      closeTitle: '',
      retryTitle: '',
      touchOutsideToDismiss: true,
    };
    this.show = this.show.bind(this);
    this.dismiss = this.dismiss.bind(this);
    this.backHandler = this.backHandler.bind(this);
  }

  async onBack() {
    const {isShow} = this.state;
    if (isShow) {
      const {retryCb} = this.state;
      retryCb();
      this.dismiss();
    }
  }

  /**
   * Handle back handle on Android
   */
  backHandler() {
    this.onBack(); // Work best with async
    return true;
  }

  _renderBody() {
    const {title, subtitle, icon, retryCb} = this.state;
    const iconSource = icon || Images.notFound;

    const iconWidth = 150;
    const iconHeight = 150;
    return (
      <View>
        <Text style={styles.popupTitle}>{title}</Text>
        {subtitle !== '' && (
          <Text style={styles.popupSubtitle}>{subtitle}</Text>
        )}
        <ImageBackground
          source={iconSource}
          style={{
            width: iconWidth,
            height: iconHeight,
            alignSelf: 'center',
            marginTop: 28,
            marginBottom: 37,
          }}
          resizeMode="contain"
        />
        {/* Retry button */}
        <Button
          title="Thử lại"
          full
          background="white"
          size="sm"
          border
          style={[{borderColor: '#71787F'}, styles.popupBtn]}
          textStyle={{fontSize: 14, color: 'black'}}
          onPress={() => {
            this.dismiss();
            navigationService.navigate('SelectDevice');
          }}
        />
      </View>
    );
  }

  /**
   * Show popup
   */
  show(params) {
    const {isShow} = this.state;
    if (isShow) {
      console.warn(
        'A Popup is already displaying. Cannot override',
        this.state,
      );
      return;
    }
    this.setState({
      isShow: true,
      ...params,
    });
    if (Platform.OS === 'android') {
      BackHandler.addEventListener('hardwareBackPress', this.backHandler);
    }
  }

  /**
   * Dismiss popup
   */
  dismiss() {
    this.setState({isShow: false});
    if (Platform.OS === 'android') {
      BackHandler.removeEventListener('hardwareBackPress', this.backHandler);
    }
  }

  render() {
    const {isShow, touchOutsideToDismiss} = this.state;
    return (
      <Dialog
        visible={isShow}
        onTouchOutside={() => {
          if (touchOutsideToDismiss) {
            this.dismiss();
          }
        }}
        dialogAnimation={
          new SlideAnimation({
            slideFrom: 'bottom',
          })
        }
        width={0.85}
        dialogStyle={styles.wrapPopup}
        overlayBackgroundColor="gray"
        overlayOpacity={0.8}>
        <DialogContent style={styles.wrapPopupContent}>
          {this._renderBody()}
        </DialogContent>
      </Dialog>
    );
  }
}

export default Component;

/**
 * Component ref
 */
let ref;

/**
 * Set ref for component
 * @param {*} _ref
 */
const setRef = _ref => {
  ref = _ref;
};

/**
 * Show popup
 * @param {*} type "succeed" or "fail"
 * @param {*} title
 * @param {*} subtitle
 * @param {*} icon require("some_icon") or null
 * @param {*} retryCb
 * @param {*} cancelCb
 * @param closeTitle
 * @param retryTitle
 * @param touchOutsideToDismiss
 * @param hiddenButtonTry
 * @param iconSize - { width, height }
 */
const responsePopup = ({
  title = 'Title here',
  subtitle = '',
  icon = null,
  retryCb = () => false,
  closeTitle = '',
  retryTitle = '',
  touchOutsideToDismiss = true,
  iconSize = null,
}) => {
  if (ref) {
    ref.show({
      title,
      subtitle,
      icon,
      retryCb,
      closeTitle,
      retryTitle,
      touchOutsideToDismiss,
      iconSize,
    });
  } else {
    console.warn('Popup not has ref');
  }
};

/**
 * Dismiss popup
 */
const dismiss = () => {
  if (ref) {
    ref.dismiss();
  } else {
    console.warn('Popup not has ref');
  }
};

export {setRef, responsePopup, dismiss};
