/**
 * @flow
 */
import React from 'react';
// import Slider from 'react-native-slider';
import Slider from '../BaseSlider';
// import Slider from '@react-native-community/slider';

import Images from '../../assets/Images';
import colors from '../../theme/colors';
import { Image, View, Platform } from 'react-native';

const MAX_VALUE = 3;
const MIN_VALUE = 1;
const STEP = 1;

type Props = {
  onSlidingComplete: (value: number, index: number) => void;
  disabled: boolean;
  style?: Record<string, any>;
  value: number;
  onSlidingStart: (value: number) => void;
  maxValue?: number;
  minValue?: number;
  index?: number;
};
type States = {};

class Component extends React.PureComponent<Props, States> {
  static defaultProps = {
    style: {},
    maxValue: MAX_VALUE,
    minValue: MIN_VALUE,
  };

  constructor(props: any) {
    super(props);

    const { value, onSlidingComplete, minValue, maxValue, index } = props;
    if (value < minValue) {
      onSlidingComplete(minValue);
    } else if (value > maxValue) {
      console.log('inhere', maxValue)
      onSlidingComplete(maxValue);
    }
  }
  /*
    componentDidUpdate(nextProps: Props) {
      console.log('inhere11',maxValue)
      const {value, onSlidingComplete, minValue, maxValue} = this.props;
      if (nextProps.value !== value) {
        console.log('inhere11',maxValue)
        if (nextProps.value < minValue) {
          onSlidingComplete(minValue);
        } else if (nextProps.value > maxValue) {
          onSlidingComplete(maxValue);
        }
      }
    }
  */
  componentWidth = 0;

  render() {
    const {
      onSlidingComplete,
      disabled,
      style,
      value,
      onSlidingStart,
      maxValue,
      minValue,
      index,
    } = this.props;
    console.log(this.props)
    return (
      <View style={{ top: 10 }}>
        <Slider
          onLayout={e => {
            this.componentWidth = e.nativeEvent.layout.width;
          }}
          onResponderGrant={e => {
            if (Platform.OS === 'ios') {
              const newValue =
                (e.nativeEvent.locationX / this.componentWidth) *
                (maxValue - minValue);
              const realValue = maxValue - newValue;
              console.log('in here');
              onSlidingComplete(realValue);
            }
          }}
          onSlidingStart={onSlidingStart}

          value={maxValue - value}
          style={{ zIndex: 1, marginHorizontal: Platform.select({ android: 140, ios: 130 }), marginVertical: Platform.select({ android: 20, ios: 20 }) }}
          disabled={disabled}
          onSlidingComplete={newValue => {
            const realValue = maxValue - newValue;
            onSlidingComplete(realValue, index);
          }}
          trackStyle={{ height: 15, borderRadius: 11 }}
          minimumValue={minValue - minValue}
          maximumValue={maxValue - minValue}
          step={STEP}
          minimumTrackTintColor="#7D7E81"
          maximumTrackTintColor="#7D7E81"
          thumbImage={Images.sliderThumb}
          thumbTouchSize={{ width: 40, height: 40 }}
          thumbStyle={{ width: 37, height: 37 }}
        />
        <View style={{ flexDirection: 'row', marginTop: -100 }}>
          {(value == 3 || value == 2) && <Image source={Images.demiL}></Image>}
          {(value == 1 || value == 2) && <Image style={{ marginLeft: 'auto' }} source={Images.demiR}></Image>}
        </View>
      </View>
    );
  }
}

export default Component;
