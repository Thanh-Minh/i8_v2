/**
 * @flow
 */
import React from 'react';
import {
  View,
  Dimensions,
  FlatList,
  ScrollView,
  Platform,
  StyleSheet,
} from 'react-native';
import colorsys from 'colorsys';
import { NavigationEvents } from 'react-navigation';

import RecommendColor from '../RecommendColor';
import SpeedSlider from '../SpeedSlider';
import EffectSelect from '../EffectSelect';
import EffectDemiSelect from '../EffectDemiSelect';
import EffectDemiWS from '../EffectDemiWS';
import styles from './styles';
import { showScreenLoading } from '../ScreenLoading';
import ble from '../../services/ble';
import eventBus from '../../services/eventBus';
import {
  EVENT_BUS,
  BLE_RESPONSE_DATA_TYPE,
  LED_MODE,
  WINNER_HAU,
  I8_BLE,
  GC,
  VAIRO_DM,
  VAIRO_HAU,
  WINNER_DM2,
} from '../../constants';
import ScreenHeader from '../ScreenHeader';
import Images from '../../assets/Images';
import { HoloColorPicker as ColorPicker, } from '../ColorPicker/src/HoloColorPicker'
import { fromHsv } from '../ColorPicker/src/utils'

const REMAP_COLOR_LIST = [
  { fake: '#eb1f2a', real: '#ff0000' },
  { fake: '#01a654', real: '#00ff00' },
  { fake: '#2e3094', real: '#0000ff' },
  { fake: '#ffff02', real: '#ff2d00' },
  { fake: '#fe69b8', real: '#ff0a50' },
  { fake: '#ffffff', real: '#ffffff' },
  { fake: '#00ffff', real: '#00ffff' },
  { fake: '#7f00fe', real: '#ff0096' },
  { fake: '#ffa400', real: '#ff3c00' },
  { fake: '#8df28e', real: '#00ff02' },
];

const RECOMMEND_COLOR_LIST_1 = REMAP_COLOR_LIST.slice(0, 5);

const RECOMMEND_COLOR_LIST_2 = REMAP_COLOR_LIST.slice(5);

const FLAT_LIST_ITEM_WIDTH = 55;
const FLAT_LIST_SEPARATOR_WIDTH = 25;
const FLAT_LIST_HEADER_WIDTH = 25;
const FLAT_LIST_FOOTER_WIDTH = 25;

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;
const CIRCLE_COLOR_PADDING = DEVICE_WIDTH < 350 ? 35 : 45;
const PREFER_SIZE_CIRCLE_PICKER =
  (DEVICE_HEIGHT / DEVICE_WIDTH / 2.16) * DEVICE_WIDTH;
const CIRCLE_COLOR_SIZE =
  ((PREFER_SIZE_CIRCLE_PICKER - CIRCLE_COLOR_PADDING) * 6) / 7;
const COLOR_THUMB_WIDTH = CIRCLE_COLOR_SIZE * 0.5;
const MAX_COLUMNS = 5;

const INITIAL_COLOR = '#ff0000';

type Props = {
  effectList: any[];
  onWillFocus?: Function;
  title: string;
  mode: number;
  maxColumns?: number;
  speedConfigurable?: boolean;
  showModeList?: boolean;
  ItemSeparatorComponent?: any;
  ListHeaderComponent?: any;
  ListFooterComponent?: any;
  modeInFlatList?: boolean;
};
type States = {};

class Component extends React.PureComponent<Props, States> {
  static defaultProps = {
    maxColumns: MAX_COLUMNS,
    onWillFocus: () => { },
    speedConfigurable: true,
    showModeList: true,
    modeInFlatList: false,
    ItemSeparatorComponent: () => (
      <View style={{ width: FLAT_LIST_SEPARATOR_WIDTH }} />
    ),
    ListHeaderComponent: () => <View style={{ width: FLAT_LIST_HEADER_WIDTH }} />,
    ListFooterComponent: () => <View style={{ width: FLAT_LIST_FOOTER_WIDTH }} />,
  };

  constructor(props: any) {
    super(props);
    const { effectList } = props;
    this.state = {
      currentColor: INITIAL_COLOR,
      selectedEffect: 1,
      currentSpeed: 51,
      effectList,
      onSlidingOrWheeling: false,
      demiMode: 1,
    };
  }

  componentDidMount() {
    eventBus.addListener(
      EVENT_BUS.CHANGE_MODE_FROM_HARDWARE,
      this.onChangeModeFromHardware,
    );
    setTimeout(() => {
      this.colorWheel && this.colorWheel.measureOffset();
    }, 500);
  }

  componentWillUnmount() {
    eventBus.removeListener(this.onChangeModeFromHardware);
  }

  onChangeModeFromHardware = currentModeData => {
    const { title } = this.props;
    console.log(title, 'aloonChangeModeFromHardware');
    const {
      mode: nextMode,
      color: colorRGB,
      speed: currentSpeed,
      effect: selectedEffect,
    } = ble.extractDataFromResponse(
      currentModeData,
      BLE_RESPONSE_DATA_TYPE.CURRENT_MODE,
    );
    const { mode } = this.props;
    console.log("mode:", mode, nextMode);
    if (mode === nextMode) {
      // Update screen following currentModeData
      console.log("COLOR THIS", colorsys.rgb2Hex(colorRGB[0], colorRGB[1], colorRGB[2])),
        colorsys.rgb2Hex(colorRGB[0], colorRGB[1], colorRGB[2]);

      const colorStr = this.isColorNeedFake(
        colorsys.rgb2Hex(colorRGB[0], colorRGB[1], colorRGB[2]),
      );
      console.log("IN HERE")
      const { effectList } = this.state;
      console.log("IN HERE32")
      const { color, speed } = effectList[selectedEffect - 1];
      console.log("IN HERE22")
      this.setState({
        currentColor: colorStr,
        currentSpeed,
        selectedEffect,
        speed,
        color,
        effectList: [...effectList], // force FlatList update
      });
      this.scrollToIndex(selectedEffect - 1);
    }
  };

  sendConfig = async configData => {
    console.log('sendConfig', configData);
    await showScreenLoading(true);
    try {
      await ble.sendData({ data: configData });
    } catch (e) {
      console.warn('sendConfig', e);
    }
    await showScreenLoading(false);
  };

  onRecommendColorSelected = color => {
    console.log('onRecommendColorSelected', color);
    const { currentColor } = this.state;
    if (currentColor === color) {
      return;
    }

    const fakeColor = this.isColorNeedFake(color);
    this.setState({ currentColor: fakeColor });

    const configData = ble.generateCommand.switchColor(color);
    this.sendConfig(configData);
  };

  onSlidingStart = () => {
    this.setState({ onSlidingOrWheeling: true });
  };

  onSlidingComplete = value => {
    console.log('onSlidingComplete------------', value);
    const { currentSpeed } = this.state;
    if (currentSpeed === value) {
      return;
    }

    this.setState({ currentSpeed: value, onSlidingOrWheeling: false });
    const configData = ble.generateCommand.switchSpeed(value);
    this.sendConfig(configData);
  };



  scrollToIndex = (index: number) => {
    const { effectList } = this.props;
    let viewPosition;
    if (Platform.OS === 'android') {
      viewPosition = 0;
    } else {
      viewPosition = -0.5 + (0.9 * (index + 1)) / effectList.length;
    }

    this.flatList &&
      this.flatList.scrollToIndex({
        index,
        viewPosition,
        // to middle of screen
        viewOffset:
          DEVICE_WIDTH / 2 -
          FLAT_LIST_HEADER_WIDTH - //header width
          FLAT_LIST_ITEM_WIDTH / 2,
        // FLAT_LIST_SEPARATOR_WIDTH
      });
  };

  onChangeEffect = async (effect, index) => {

    console.log('onChangeEffect -----------------', effect, index);
    const { selectedEffect } = this.state;
    if (selectedEffect === effect) {
      return;
    }

    const { mode } = this.props;
    if (mode === LED_MODE.DEMI && (ble.getDeviceName() === VAIRO_DM || ble.getDeviceName() === WINNER_DM2)) {

      index = effect - 1;
      console.log('onChangeEffect11 -----------------', effect, index);
    }
    const { effectList } = this.state;

    const { color, speed } = effectList[index];
    this.setState({
      selectedEffect: effect,
      speed,
      color,
      effectList: [...effectList], // force FlatList update
    });

    this.scrollToIndex(index);
    const configData = ble.generateCommand.switchEffect(effect, mode);
    await this.sendConfig(configData);
    // Ask current effect data to update UI
    await showScreenLoading(true);
    try {
      const feedbackData = await ble.sendData({
        data: ble.generateCommand.askCurrentEffect(),
        waitForResponse: true,
      });
      const { color: colorRGB, speed } = await ble.extractDataFromResponse(
        feedbackData,
        BLE_RESPONSE_DATA_TYPE.CURRENT_MODE,
      );
      const colorStr = this.isColorNeedFake(
        colorsys.rgb2Hex(colorRGB[0], colorRGB[1], colorRGB[2]),
      );
      this.setState({ currentColor: colorStr, currentSpeed: speed });
    } catch (e) {
      console.warn('onChangeEffect', e);
    }
    await showScreenLoading(false);
  };

  onChangeColorPicker = async color => {
    console.log('onChangeColorPicker', color);
    const currentColor = fromHsv(color);
    this.setState({ currentColor });
    const configData = ble.generateCommand.switchColor(currentColor);
    this.sendConfig(configData);
  };

  isColorNeedFake = color => {
    let fakeColor;
    for (let i = 0; i < REMAP_COLOR_LIST.length; i++) {
      const value = REMAP_COLOR_LIST[i];
      if (value.real === color) {
        fakeColor = value.fake;
      }
    }
    return fakeColor || color;
  };

  render() {
    const {
      title,
      onWillFocus,
      speedConfigurable,
      showModeList,
      ItemSeparatorComponent,
      ListHeaderComponent,
      ListFooterComponent,
      modeInFlatList,
      mode,
    } = this.props;
    const {
      currentColor,
      selectedEffect,
      speed,
      color,
      currentSpeed,
      effectList,
    } = this.state;
    console.log("IN HERE11")
    return (
      <View style={{ flex: 1 }}>
        <NavigationEvents
          onWillBlur={payload => {
            payload.state.params = undefined; // Reset navigation params
          }}
          onWillFocus={async payload => {
            console.log('onWillFocus', title);
            let currentModeData;
            // Open first time when open app
            if (payload.state.params && payload.state.params.currentModeData) {
              ({ currentModeData } = payload.state.params);
            } else {
              currentModeData = await onWillFocus();
            }
            console.log('onWillFocus', 'currentModeData', currentModeData);
            // Set up State based on currentEffectData
            const {
              color: colorRGB,
              speed: currentSpeed,
              effect,
            } = currentModeData;
            const colorStr = this.isColorNeedFake(
              colorsys.rgb2Hex(colorRGB[0], colorRGB[1], colorRGB[2]),
            );

            let color, speed;
            effectList.forEach(value => {
              if (value.effect === effect) {
                ({ color, speed } = value);
              }
            });
            this.setState({
              currentColor: colorStr,
              currentSpeed,
              selectedEffect: effect,
              color,
              speed,
              effectList: [...effectList], // force FlatList update
            });
          }}
        />
        {/* HEADER */}
        <ScreenHeader title={title} />

        <ScrollView
          bounces={false}
          scrollEventThrottle={5}
          onScroll={event => {
            this.colorWheel && this.colorWheel.measureOffset();
            this.colorPicker && this.colorPicker.measurePosition();
          }}
          showsVerticalScrollIndicator={false}
          scrollEnabled={!this.state.onSlidingOrWheeling}>
          {/* Demi effect select */}
          {/* DemiV2 -- tam dung I8 de test*/
            console.log('mode', mode, 'nameDevice', ble.getDeviceName())}
          {mode === LED_MODE.DEMI && (ble.getDeviceName() === VAIRO_DM || ble.getDeviceName() === WINNER_DM2) ? (
            <View >
              {console.log('effectList--------', effectList)}
              <EffectDemiWS
                //index={index}
                onSlidingStart={this.onSlidingStart}
                value={selectedEffect}
                //disabled={!selectedEffect}
                onSlidingComplete={this.onChangeEffect}
              />
            </View>
          ) : null}
          {/*hau winnerX*/}
          {mode === LED_MODE.DEMI && (ble.getDeviceName() === WINNER_HAU || ble.getDeviceName() === VAIRO_HAU || ble.getDeviceName() === GC) ? (
            <View
              style={{
                width: '100%',
                flexDirection: 'row',
                justifyContent: 'space-evenly',
                marginTop: 36,
              }}>
              {console.log("11:", effectList)}
              {effectList.map((value, index) => (
                <EffectDemiSelect
                  index={index}
                  key={value.effect}
                  effect={value.effect}
                  name={value.name}
                  selectedEffect={selectedEffect}
                  onChangeEffect={this.onChangeEffect}
                />
              ))}
            </View>
          ) : null}

          {/* Color picker */}
          <View style={[styles.colorPicker]}>
            <View
              style={[
                StyleSheet.absoluteFillObject,
                styles.colorIndicatorWrapper,
              ]}>
              <View
                style={[
                  styles.circlePicked,
                  {
                    backgroundColor: currentColor,
                    height: COLOR_THUMB_WIDTH,
                    width: COLOR_THUMB_WIDTH,
                    borderRadius: COLOR_THUMB_WIDTH,
                  },
                  {
                    opacity: color ? 1 : 0.5,
                  },
                ]}
              />
            </View>
            <View
              style={{
                width: CIRCLE_COLOR_SIZE,
                height: CIRCLE_COLOR_SIZE,
                alignItems: 'center',
                justifyContent: 'center',
                borderRadius: CIRCLE_COLOR_SIZE,
                borderWidth: 2,
                borderColor: '#3F3F41',
              }}>
              <ColorPicker
                ref={ref => {
                  this.colorPicker = ref;
                }}
                onWheelStart={() => this.setState({ onSlidingOrWheeling: true })}
                onWheelStop={() => this.setState({ onSlidingOrWheeling: false })}
                hideSliders
                enable={color}
                defaultColor={this.state.currentColor}
                onColorChange={color => {
                  const currentColor = fromHsv(color);
                  this.setState({ currentColor });
                }}
                onColorChangeComplete={color => {
                  this.onChangeColorPicker(color)
                }}
                style={{
                  width: CIRCLE_COLOR_SIZE,
                  height: CIRCLE_COLOR_SIZE
                }}
              />
            </View>
          </View>
          {/* Recommend Color */}
          <View style={[{ opacity: color ? 1 : 0.5 }, styles.configWrapper]}>
            {/* <FunctionTitle title="MÀU CƠ BẢN" /> */}
            <View style={styles.groupColor1}>
              {RECOMMEND_COLOR_LIST_1.map((_color, index) => {
                let image;
                switch (index + 1) {
                  case 1:
                    image = Images.c1;
                    break;
                  case 2:
                    image = Images.c2;
                    break;
                  case 3:
                    image = Images.c3;
                    break;
                  case 4:
                    image = Images.c4;
                    break;
                  case 5:
                    image = Images.c5;
                    break;

                  default:
                    image = null;
                    break;
                }
                return (
                  <RecommendColor
                    image={image}
                    disabled={!color}
                    key={index.toString()}
                    colorDisplay={_color.fake}
                    colorReal={_color.real}
                    onPress={this.onRecommendColorSelected}
                  />
                );
              })}
            </View>
            <View style={styles.groupColor2}>
              {RECOMMEND_COLOR_LIST_2.map((_color, index) => {
                let image;
                switch (index + 6) {
                  case 6:
                    image = Images.c6;
                    break;
                  case 7:
                    image = Images.c7;
                    break;
                  case 8:
                    image = Images.c8;
                    break;
                  case 9:
                    image = Images.c9;
                    break;
                  case 10:
                    image = Images.c10;
                    break;

                  default:
                    image = null;
                    break;
                }
                return (
                  <RecommendColor
                    image={image}
                    disabled={!color}
                    key={index.toString()}
                    colorDisplay={_color.fake}
                    colorReal={_color.real}
                    onPress={this.onRecommendColorSelected}
                  />
                );
              })}
            </View>
          </View>
          {/* Speed slider */}
          {speedConfigurable && (
            <View style={[{ opacity: speed ? 1 : 0.5 }, styles.configWrapper]}>
              {/* <FunctionTitle title="TỐC ĐỘ" /> */}
              <SpeedSlider
                minValue={
                  mode === LED_MODE.BREAK
                    ? Platform.select({ ios: 50 })
                    : undefined
                }
                onSlidingStart={this.onSlidingStart}
                value={currentSpeed}
                disabled={!speed}
                onSlidingComplete={this.onSlidingComplete}
              />
            </View>
          )}
          {/* Mode */}
          {showModeList && (
            <View style={styles.configWrapper}>
              {/* <FunctionTitle title="HIỆU ỨNG" /> */}
              {modeInFlatList && (
                <FlatList
                  bounces={false}
                  ref={ref => {
                    this.flatList = ref;
                  }}
                  ItemSeparatorComponent={ItemSeparatorComponent}
                  ListHeaderComponent={ListHeaderComponent}
                  ListFooterComponent={ListFooterComponent}
                  horizontal
                  showsVerticalScrollIndicator={false}
                  showsHorizontalScrollIndicator={false}
                  data={effectList}
                  getItemLayout={(data, index) => ({
                    length: FLAT_LIST_ITEM_WIDTH,
                    offset:
                      FLAT_LIST_ITEM_WIDTH * index +
                      FLAT_LIST_SEPARATOR_WIDTH * (index ? index : index),
                    index,
                  })}
                  renderItem={({ item: value, index }) => {
                    return (
                      <EffectSelect
                        index={index}
                        key={value.effect}
                        effect={value.effect}
                        name={value.name}
                        selectedEffect={selectedEffect}
                        onChangeEffect={this.onChangeEffect}
                      />
                    );
                  }}
                  keyExtractor={(item, index) => index.toString()}
                />
              )}
              {modeInFlatList || (
                <View
                  style={{
                    width: '100%',
                    flexDirection: 'row',
                    justifyContent: 'space-evenly',
                  }}>
                  {effectList.map((value, index) => (
                    <EffectSelect
                      index={index}
                      key={value.effect}
                      effect={value.effect}
                      name={value.name}
                      selectedEffect={selectedEffect}
                      onChangeEffect={this.onChangeEffect}
                    />
                  ))}
                </View>
              )}
            </View>
          )}
          <View style={{ height: 5 }} />
        </ScrollView>
      </View>
    );
  }
}

export default Component;
