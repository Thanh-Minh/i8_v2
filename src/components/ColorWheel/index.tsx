// @flow

import React from 'react';
import {
  Animated,
  Image,
  Dimensions,
  PanResponder,
  StyleSheet,
  View,
  Platform,
} from 'react-native';
import colorsys from 'colorsys';

const UPDATE_COLOR_FREQUENCY = Platform.select({ios: 20, android: 1});
const UPDATE_THUMB_POS_FREQUENCY = Platform.select({ios: 2, android: 1});
const DELAY_TO_DETERMINE_TOUCH = 200;

export default class ColorWheel extends React.PureComponent {
  sample = 0;
  thumbSample = 0;
  spinValue = new Animated.Value(0);

  static defaultProps = {
    thumbSize: 26,
    initialColor: '#ff0000',
    onColorChange: () => {},
    precision: 0,
  };

  constructor(props) {
    super(props);
    this.state = {
      offset: {x: 0, y: 0},
      currentColor: props.initialColor,
      pan: new Animated.ValueXY(),
      radius: 100,
    };
  }

  startTouchTime = 0;

  countOnLayout = 0;

  componentDidMount = () => {
    this._panResponder = PanResponder.create({
      onStartShouldSetPanResponderCapture: ({nativeEvent}) => {
        const {enable} = this.props;
        if (!enable) {
          return false;
        }

        if (this.outBounds(nativeEvent)) return;
        this.updateColor({nativeEvent});
        this.setState({panHandlerReady: true});

        this.state.pan.setValue({
          x: -this.state.left + nativeEvent.pageX - this.props.thumbSize / 2,
          y: -this.state.top + nativeEvent.pageY - this.props.thumbSize / 2,
        });
        this.sample = 0;
        this.thumbSample = 0;
        return true;
      },
      onStartShouldSetPanResponder: () => {
        const {enable} = this.props;
        if (!enable) {
          return false;
        }
        return true;
      },
      onMoveShouldSetPanResponderCapture: () => {
        const {enable} = this.props;
        if (!enable) {
          return false;
        }
        return true;
      },
      onPanResponderGrant: () => {
        const {enable} = this.props;
        if (!enable) {
          return false;
        }
        this.startTouchTime = Date.now();
        this.props.onWheelStart();
        return true;
      },
      onPanResponderMove: (event, gestureState) => {
        if (this.outBounds(gestureState)) {
          return;
        }
        const now = Date.now();

        if (now - this.startTouchTime < DELAY_TO_DETERMINE_TOUCH) {
          return;
        }

        this.resetPanHandler();

        const {deg} = this.calcPolar(gestureState);
        const radius = 1; // Force radius always 1
        const currentColor = colorsys.hsv2Hex({
          h: deg,
          s: 100 * radius,
          v: 100,
        });
        this.thumbSample += 1;
        // Reduce Update thumb color and position -> Optimize performance
        if (this.thumbSample >= UPDATE_THUMB_POS_FREQUENCY) {
          this.setState({currentColor});
          this.spinValue.setValue(deg);
          this.thumbSample = 0;
        }

        return Animated.event(
          [
            null,
            {
              dx: this.state.pan.x,
              dy: this.state.pan.y,
            },
          ],
          {listener: this.updateColor},
        )(event, gestureState);
      },
      onMoveShouldSetPanResponder: () => {
        const {enable} = this.props;
        if (!enable) {
          return false;
        }
        return true;
      },
      onPanResponderRelease: ({nativeEvent}) => {
        this.setState({panHandlerReady: true});
        this.state.pan.flattenOffset();
        const {deg} = this.calcPolar(nativeEvent);
        const radius = 1; // Force radius always 1

        // Not expected touch area
        if (this.outBounds(nativeEvent)) {
        } else {
          // Update Color of thumb and its position
          const currentColor = colorsys.hsv2Hex({
            h: deg,
            s: 100 * radius,
            v: 100,
          });
          this.setState({currentColor});
          this.spinValue.setValue(deg);
          this.props.onColorChangeComplete({h: deg, s: 100 * radius, v: 100});
        }
        this.props.onWheelStop();
      },
    });
  };

  onLayout() {
    if (this.countOnLayout < 2) {
      this.countOnLayout++;
    }
    this.measureOffset();
  }

  /*
   * const {x, y, width, height} = nativeEvent.layout
   * onlayout values are different than measureInWindow
   * x and y are the distances to its previous element
   * but in measureInWindow they are relative to the window
   */
  measureOffset = () => {
    this.self.measureInWindow((x, y, width, height) => {
      const window = Dimensions.get('window');
      const absX = x % width;
      const radius = Math.min(width, height) / 2;
      const offset = {
        x: absX + width / 2,
        y: (y % window.height) + height / 2,
      };
      this.setState({
        offset,
        radius,
        height,
        width,
        top: y % window.height,
        left: absX,
      });
      this.forceUpdate(this.state.currentColor);
    });
  };

  calcPolar = gestureState => {
    const {pageX, pageY, moveX, moveY} = gestureState;
    const [x, y] = [pageX || moveX, pageY || moveY];
    const [dx, dy] = [x - this.state.offset.x, y - this.state.offset.y];
    return {
      deg: Math.atan2(dy, dx) * (-180 / Math.PI),
      // pitagoras r^2 = x^2 + y^2 normalized
      radius: Math.sqrt(dy * dy + dx * dx) / this.state.radius,
    };
  };

  outBounds = gestureState => {
    const {radius} = this.calcPolar(gestureState);
    return radius > 1.2 || radius < 0.8;
  };

  resetPanHandler = () => {
    if (!this.state.panHandlerReady) {
      return;
    }

    this.setState({panHandlerReady: false});
    this.state.pan.setOffset({
      x: this.state.pan.x._value,
      y: this.state.pan.y._value,
    });
    this.state.pan.setValue({x: 0, y: 0});
  };

  calcCartesian = (deg, radius) => {
    const r = radius * this.state.radius < 1 ? 0 : radius * this.state.radius; // was normalized
    const rad = (Math.PI * deg) / 180;
    const x = r * Math.cos(rad);
    const y = r * Math.sin(rad);

    return {
      left: this.state.width / 2 + x,
      top: this.state.height / 2 - y,
    };
  };

  updateColor = ({nativeEvent}) => {
    const {deg} = this.calcPolar(nativeEvent);
    const radius = 1; // Force radius always 1
    this.sample += 1;
    if (this.sample >= UPDATE_COLOR_FREQUENCY) {
      this.props.onColorChange({h: deg, s: 100 * radius, v: 100});
      this.sample = 0;
    }
  };

  forceUpdate = (color, fromOutside) => {
    const {h, s, v} = colorsys.hex2Hsv(color);
    const {left, top} = this.calcCartesian(h, s / 100);
    if (!fromOutside) {
      if (this.countOnLayout > 2) {
        this.props.onColorChange({h, s, v});
      }
    }
    this.state.pan.setValue({
      x: left - this.props.thumbSize / 2,
      y: top - this.props.thumbSize / 2,
    });
  };

  render() {
    const {radius} = this.state;
    const {enable} = this.props;
    const panHandlers =
      (this._panResponder && this._panResponder.panHandlers) || {};

    const spin = this.spinValue.interpolate({
      inputRange: [0, 360],
      outputRange: ['360deg', '0deg'], // rotate từ 0 đến 360 độ dựa vào input range
    });
    const thumbSize = this.props.style.width / 10;

    return (
      <View
        ref={node => {
          this.self = node;
        }}
        {...panHandlers}
        onLayout={nativeEvent => this.onLayout(nativeEvent)}
        style={[
          styles.coverResponder,
          this.props.style,
          {opacity: enable ? 1 : 0.5},
        ]}>
        <Image
          style={[
            styles.img,
            {
              height: radius * 2,
              width: radius * 2,
            },
          ]}
          source={require('./color-wheel.png')}
        />
        <View
          style={[
            StyleSheet.absoluteFill,
            {justifyContent: 'center', alignItems: 'center'},
          ]}>
          <Animated.View
            style={[
              {
                width: '100%',
                alignItems: 'flex-end',
              },
              {transform: [{rotateZ: spin}]},
            ]}>
            <View
              style={{
                width: thumbSize,
                height: thumbSize,
                borderRadius: thumbSize,
                backgroundColor: this.state.currentColor,
                marginRight: (-thumbSize / 24) * 7,
              }}
            />
          </Animated.View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  coverResponder: {
    zIndex: 10,
  },
  img: {
    alignSelf: 'center',
    transform: [{rotateZ: '90deg'}, {scaleX: -1}],
  },
  circle: {},
});
