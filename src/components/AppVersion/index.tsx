/**
 * @flow
 */
import React from 'react';
import {Text} from 'react-native';
import DeviceInfo from 'react-native-device-info';

class AppVersion extends React.PureComponent<
  {},
  {isLoading: boolean; version: string; build: string; jsVer: string}
> {
  constructor() {
    super();
    this.state = {isLoading: true, version: '', build: ''};
  }
  async componentDidMount() {
    // Get version and build
    const version = DeviceInfo.getVersion();
    const build = DeviceInfo.getBuildNumber();

    this.setState({
      isLoading: false,
      version,
      build,
    });
  }
  render() {
    const {isLoading, version, build} = this.state;
    if (isLoading) {
      return null;
    } else {
      return (
        <Text
          style={{
            fontSize: 12,
            width: '100%',
            textAlign: 'center',
            color: '#808080',
          }}>
          {`Version ${version} (${build})`}
        </Text>
      );
    }
  }
}
export default AppVersion;
