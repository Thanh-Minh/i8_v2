import React from 'react';
import {View, StatusBar, SafeAreaView} from 'react-native';

import colors from '../../theme/colors';

const withSafeArea = ({
  forceInset,
  style,
  bottomColor,
  topColor,
  barStyle,
} = {}) => Component =>
  class extends React.PureComponent {
    render() {
      const {props} = this;
      return (
        <View
          style={{flex: 1, backgroundColor: topColor || colors.secondaryDark}}>
          <SafeAreaView forceInset={forceInset || {}} style={[{flex: 1}]}>
            <StatusBar
              backgroundColor={topColor || colors.secondaryDark}
              barStyle={barStyle || 'dark-content'}
            />
            {/* Override bottom color */}
            <View
              style={{
                position: 'absolute',
                bottom: 0,
                left: 0,
                height: 100,
                width: '100%',
                backgroundColor: bottomColor || 'transparent',
              }}
            />
            <View style={[{flex: 1}, style]}>
              <Component {...props} />
            </View>
          </SafeAreaView>
        </View>
      );
    }
  };

export default withSafeArea;
