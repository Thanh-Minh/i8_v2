/**
 * @flow
 */
import React from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import colors from '../../theme/colors';
import Images from '../../assets/Images';

type Props = {};
type States = {};

class Component extends React.PureComponent<Props, States> {
  constructor(props: any) {
    super(props);
  }

  render() {
    const {effect, selectedEffect, name, onChangeEffect, index} = this.props;
    const isActive = selectedEffect === effect;
    let iconSource = null;
    switch (name) {
      case 'DEMI 1':
        iconSource = isActive ? Images.demiOneActive : Images.demiOne;
        break;
      case 'DEMI 2':
        iconSource = isActive ? Images.demiTwoActive : Images.demiTwo;
        break;
      default:
        break;
    }

    return (
      <TouchableOpacity
        hitSlop={{top: 10, right: 10, left: 10, bottom: 10}}
        style={{
          // borderWidth: 1,
          // minHeight: 40,
          // minWidth: 120,
          // paddingHorizontal: 15,
          // borderColor: '#707070',
          justifyContent: 'center',
          alignItems: 'center',
          // borderRadius: 20,
          // backgroundColor:
          //   effect === selectedEffect ? colors.mainLight : 'transparent',
        }}
        onPress={() => onChangeEffect(effect, index)}>
        <Image
          source={iconSource}
          style={{height: 41, width: 113, resizeMode: 'contain'}}
        />
      </TouchableOpacity>
    );
  }
}

export default Component;
