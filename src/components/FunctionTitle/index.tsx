import React from 'react';
import {View, Text, ViewStyle} from 'react-native';

import styles from './styles';

type Props = {title: string; style: ViewStyle};
type States = {};

class FunctionTitle extends React.PureComponent<Props, States> {
  render() {
    const {title, style} = this.props;
    return (
      <View style={[styles.container, style]}>
        <View style={styles.line} />
        <View style={styles.titleContainer}>
          <Text style={styles.title}>{title}</Text>
        </View>
        <View style={styles.line} />
      </View>
    );
  }
}

export default FunctionTitle;
