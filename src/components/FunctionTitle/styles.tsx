import {StyleSheet} from 'react-native';
import colors from '../../theme/colors';

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    width: '100%',
    alignItems: 'center',
    marginVertical: 5,
  },
  line: {
    flex: 1,
    height: 1,
    backgroundColor: colors.mainLight,
  },
  titleContainer: {
    padding: 5,
    borderColor: colors.mainLight,
    borderRadius: 40,
    borderWidth: 1,
    minWidth: 200,
    alignItems: 'center',
  },
  title: {
    fontSize: 14,
    color: colors.mainLight,
  },
});
