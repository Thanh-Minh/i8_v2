/**
 * @flow
 */
import React from 'react';
import {ActivityIndicator, StyleSheet, View} from 'react-native';

let ref;
class ScreenLoading extends React.PureComponent {
  constructor() {
    super();
    this.state = {isLoading: false};
  }

  render() {
    const {isLoading} = this.state;
    if (isLoading) {
      return (
        <View
          style={[
            StyleSheet.absoluteFill,
            {
              justifyContent: 'center',
              alignItems: 'center',
            },
          ]}>
          <View
            style={[
              StyleSheet.absoluteFill,
              // { backgroundColor: "gray", opacity: 0.7 }
            ]}
          />
          <ActivityIndicator size="large" color="green" />
        </View>
      );
    }
    return null;
  }
}

const setRef = _ref => (ref = _ref);

const showScreenLoading = isLoading => {
  if(ref) ref.setState({isLoading});
};

export {setRef, showScreenLoading};

export default ScreenLoading;
