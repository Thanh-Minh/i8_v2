/** @flow */
import React from 'react';
import {Text, ImageBackground, TouchableOpacity} from 'react-native';

type ButtonWithImageProps = {
  title: string;
  marginBottom?: number;
  marginTop?: number;
  iconSource?: any;
  onPress?: Function;
};

const ButtonOfDrawer = ({
  title,
  marginBottom,
  marginTop,
  iconSource,
  onPress,
}: ButtonWithImageProps) => (
  <TouchableOpacity
    style={{
      minHeight: 44,
      flexDirection: 'row',
      paddingLeft: 32,
      marginBottom: marginBottom,
      marginTop: marginTop,
      alignItems: 'center',
    }}
    onPress={onPress}>
    <ImageBackground
      style={{width: 38, height: 38}}
      imageStyle={{
        resizeMode: 'contain',
      }}
      source={iconSource}
    />
    <Text
      style={[
        {
          fontSize: 20,
          marginLeft: 37,
        },
      ]}>
      {title}
    </Text>
  </TouchableOpacity>
);

ButtonOfDrawer.defaultProps = {
  marginTop: 10,
  marginBottom: 10,
  iconSource: null,
  onPress: () => false,
};

export default ButtonOfDrawer;
