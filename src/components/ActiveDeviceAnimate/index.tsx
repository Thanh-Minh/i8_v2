/**
 * @flow
 */
import React from 'react';
import {View, Animated, Easing, ImageBackground} from 'react-native';

import Images from '../../assets/Images';

const CIRCLE_COUNT = 3;

type Props = {size: number};
type States = {};

class ActiveDeviceAnimate extends React.PureComponent<Props, States> {
  animateValueArr = [];

  constructor(props: any) {
    super(props);
    for (let i = 0; i < CIRCLE_COUNT; i++) {
      this.animateValueArr.push(new Animated.Value(0));
    }
  }

  componentDidMount() {
    // this.animateValueArr.forEach(value => {
    //   this.animate(value);
    // });
    this.animateStagger();
  }

  animate(value: any) {
    value.setValue(0);
    Animated.timing(value, {
      toValue: 1.5,
      duration: 2000,
      easing: Easing.linear,
    }).start(() => {
      this.animate(value);
    });
  }

  animateStagger() {
    const config = {
      toValue: 1.5,
      duration: 2000,
      easing: Easing.linear,
    };
    Animated.stagger(
      800,
      this.animateValueArr.map(value => {
        value.setValue(0);
        return Animated.timing(value, config);
      }),
    ).start(() => {
      this.animateStagger();
    });
  }

  render() {
    const {size} = this.props;
    const minSize = (size * 160) / 265;
    const maxSize = size;
    return (
      <View
        style={[
          {
            alignSelf: 'center',
            justifyContent: 'center',
            height: maxSize,
            width: maxSize,
            margin: 40,
          },
        ]}>
        {this.animateValueArr.map((value, index) => {
          const size = this.animateValueArr[index].interpolate({
            inputRange: [0, 1],
            outputRange: [minSize, maxSize],
          });
          const borderWidth = this.animateValueArr[index].interpolate({
            inputRange: [0, 0.1, 1],
            outputRange: [0, 0.8, 1.5],
          });
          const opacity = this.animateValueArr[index].interpolate({
            inputRange: [0, 1],
            outputRange: [1, 0.3],
          });
          return (
            <Animated.View
              key={index.toString()}
              style={[
                {
                  position: 'absolute',
                  alignSelf: 'center',
                  borderColor: 'green',
                  borderRadius: maxSize,
                },
                {
                  //   Animated attributes
                  borderWidth: borderWidth,
                  width: size,
                  height: size,
                  opacity,
                },
              ]}
            />
          );
        })}
        <ImageBackground
          style={{
            width: 160,
            height: 85,
            alignSelf: 'center',
          }}
          resizeMode="contain"
          source={Images.bleScan}
        />
      </View>
    );
  }
}

export default ActiveDeviceAnimate;
