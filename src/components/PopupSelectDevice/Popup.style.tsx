import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  wrapPopup: {
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  wrapPopupContent: {
    backgroundColor: 'white',
    borderRadius: 5,
    overflow: 'hidden',
  },
  header: {
    backgroundColor: '#202126',
    paddingHorizontal: 15,
    height: 40,
    justifyContent: 'center',
  },
  headerText: {
    color: 'white',
  },
  contentWrapper: {
    marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'space-around',
    minWidth: '80%',
    paddingHorizontal: 20,
  },

  deviceWrapper: {
    borderRadius: 10,
    borderWidth: 1,
    borderColor: 'black',
    height: 120,
    width: 120,
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconName: {width: 110, height: 110, resizeMode: 'contain'},

  btnCancel: {
    marginTop: 40,
    marginBottom: 15,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: 'gray',
    height: 40,
    width: 100,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  btnCancelText: {
    color: 'gray',
  },
});
