import React from 'react';
import {
  BackHandler,
  Platform,
  View,
  Text,
  Image,
  TouchableOpacity,
} from 'react-native';
import { Dialog, DialogContent, SlideAnimation } from 'react-native-popup-dialog';

import styles from './Popup.style';
import Images from '../../assets/Images';
import { WINNER_HAU, I8_BLE } from '../../constants';

class PopupSelectDevice extends React.PureComponent {
  constructor(props: any) {
    super(props);
    this.state = {
      isShow: false,
    };
    this.selectCallback = null;
    this.cancelCallback = null;
  }

  showPopupSelect = async () => {
    return new Promise((resolve, eject) => {
      this._show();
      this.selectCallback = resolve;
      this.cancelCallback = eject;
    });
  };

  _onBack = async () => {
    const { isShow } = this.state;
    if (isShow) {
      this.cancelCallback && this.cancelCallback('cancel');
      this._dismiss();
    }
  };

  /**
   * Handle back handle on Android
   */
  _backHandler = () => {
    this._onBack(); // Work best with async
    return true;
  };

  /**
   * Show popup
   */
  _show = () => {
    const { isShow } = this.state;
    if (isShow) {
      console.warn(
        'A Popup is already displaying. Cannot override',
        this.state,
      );
      return;
    }
    this.setState({
      isShow: true,
    });
    if (Platform.OS === 'android') {
      BackHandler.addEventListener('hardwareBackPress', this._backHandler);
    }
  };

  /**
   * Dismiss popup
   */
  _dismiss = () => {
    this.setState({ isShow: false });
    if (Platform.OS === 'android') {
      BackHandler.removeEventListener('hardwareBackPress', this._backHandler);
    }
  };

  _onSelect = (deviceName: string) => {
    this.selectCallback && this.selectCallback(deviceName);
    this._dismiss();
  };

  render() {
    const { isShow } = this.state;
    return (
      <Dialog
        visible={isShow}
        onTouchOutside={() => { }}
        dialogAnimation={
          new SlideAnimation({
            slideFrom: 'bottom',
          })
        }
        width={1}
        dialogStyle={styles.wrapPopup}
        overlayBackgroundColor="gray"
        overlayOpacity={0.8}>
        {/* <DialogContent> */}
        <View style={styles.wrapPopupContent}>
          {/* Header */}
          <View style={styles.header}>
            <Text style={styles.headerText}>Chọn thiết bị</Text>
          </View>
          {/* Body */}
          <View style={styles.contentWrapper}>
            <TouchableOpacity
              style={styles.deviceWrapper}
              onPress={() => this._onSelect(I8_BLE)}>
              <Image style={styles.iconName} source={Images.logoTransparent} />
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.deviceWrapper}
              onPress={() => this._onSelect(WINNER_HAU)}>
              <Image style={styles.iconName} source={Images.logoTransparentX} />
            </TouchableOpacity>
          </View>
          {/* Bottom */}
          <TouchableOpacity
            style={styles.btnCancel}
            onPress={() => {
              this.cancelCallback && this.cancelCallback('cancel');
            }}>
            <Text style={styles.btnCancelText}>Huỷ</Text>
          </TouchableOpacity>
        </View>
        {/* </DialogContent> */}
      </Dialog>
    );
  }
}

export default PopupSelectDevice;
