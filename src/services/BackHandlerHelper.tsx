/**
 * @flow
 */
import React from 'react';
import {BackHandler} from 'react-native';

import NavigationService from './navigationService';

type Props = {
  onBackPress?: Function;
  screenName?: string;
};

class BackHandlerHelper extends React.PureComponent<Props, {}> {
  static defaultProps = {
    onBackPress: NavigationService.goBack,
    screenName: '',
  };

  static propTypes = {};

  componentDidMount() {
    const {screenName} = this.props;
    console.log('BackHandlerHelper add', screenName);
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
  }

  componentWillUnmount() {
    const {screenName} = this.props;
    console.log('BackHandlerHelper remove', screenName);
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }

  handleBackPress = () => {
    const {onBackPress, screenName} = this.props;
    console.log('handleBackPress', screenName);
    onBackPress(); // works best when the goBack is async
    return true;
  };

  render() {
    return null;
  }
}

export default BackHandlerHelper;
