/**
 * @flow
 */
import { NordicDFU, DFUEmitter } from 'react-native-nordic-dfu';
import { I8_BLE } from '../../constants';

class DfuHandler {
  static instance: DfuHandler;

  static getInstance() {
    if (!DfuHandler.instance) {
      DfuHandler.instance = new DfuHandler();
    }
    return DfuHandler.instance;
  }

  callbackDFUProgress: Function = () => { };

  callbackDFUStateChange: Function = () => { };

  handlerDiscover: any;

  /**
   * Check if HW have any Update
   * @param {*} currentVersion current version of HW known through Adver. Package
   */
  checkUpdateForHW(deviceName: string, deviceVersion: number[]) {

    let latestVer;
    switch (deviceName) {
      case 'VAIRO_DM':
      case 'VAIRO_HAU':
      case 'WINNER_HAU':
      case 'WINNER_DM2':
      case 'GC':
        latestVer = [1, 0, 0];
        break
      case 'I8_BLE':
        latestVer = [1, 0, 1];
        break
      default:
        latestVer = [1, 0, 0];
        break;


    }
    console.log("checkUpdateForHW", latestVer, deviceVersion)
    return compareVersions(latestVer, deviceVersion);
  }

  /**
   * Start DFU progress
   * @param {*} deviceAddress MAC address (Android) / UUID (iOS)
   * @param {*} filePath FW location on storage
   */
  startDFU = (deviceAddress: string, filePath: string, deviceName: string) => {
    console.log('startDFU', deviceAddress, filePath);
    NordicDFU.startDFU({
      deviceAddress,
      deviceName: deviceName,
      filePath,
    })
      .then(res => {
        console.log('Transfer done:', res);
      })
      .catch(e => {
        console.log('startDFU error ', e.code, e);
      });
  };

  /**
   * Callback for DFU progress changes
   * @param {*} callback callback for DFU progress
   * @example  ({ percent, currentPart, partsTotal, avgSpeed, speed }) => {
        console.log("DFU progress: " + percent + "%");
      }
   */
  registerDFUProgressCallback(callback: Function) {
    this.callbackDFUProgress = callback;
    DFUEmitter.addListener('DFUProgress', this.callbackDFUProgress);
  }

  /**
   * Callback when DFU state changes
   * @param {*} callback
   * @example 
   *  ({ state }) => {
        console.log("DFU State:", state);
      });
      
     @note value of state:
      - "DFU_FAILED"
   */
  registerDFUStateChangedCallback(callback: Function) {
    this.callbackDFUStateChange = callback;
    DFUEmitter.addListener('DFUStateChanged', this.callbackDFUStateChange);
  }

  unresgisterAllCallback() {
    DFUEmitter.removeListener('DFUProgress', this.callbackDFUProgress);
    DFUEmitter.removeListener('DFUStateChanged', this.callbackDFUStateChange);
  }
}
export default DfuHandler.getInstance();

/**
 * @return
 * - 1 if ver1 > ver2
 * - 0 if ver1 = ver2
 * - -1 if ver1 < ver2
 * @param ver1
 * @param ver2
 */
const compareVersions = (
  ver1: number[] | readonly number[],
  ver2: number[] | readonly number[],
) => {
  for (let i = 0; i < 3; i += 1) {
    if (ver1[i] === ver2[i]) {
      continue;
    } else if (ver1[i] > ver2[i]) {
      return 1;
    } else {
      return -1;
    }
  }
  return 0;
};
