import {
  NativeEventEmitter,
  NativeModules,
  Platform,
  PermissionsAndroid,
} from 'react-native';
import BleManager from 'react-native-ble-manager';

import {
  BLE_ERROR,
  BLE_RESPONSE_DATA_TYPE,
  I8_BLE,
  EVENT_BUS,
  WINNER_HAU,
} from '../../constants';
import eventBus from '../eventBus';

const BleManagerModule = NativeModules.BleManager;
const bleManagerEmitter = new NativeEventEmitter(BleManagerModule);

let didInit;

let isConnected = false;
let isScanning = false;
let bleOn = false;

let scanAction: 'scanOnly' | 'scanToConnect' = 'scanOnly';
let foundDeviceName: string[] = [];

let deviceAddr: string | null;
let connectedDeviceId: number[];
let deviceName: null | string = null;
let deviceVersion: number[] = [];

const SERVICE_DATA_UUID = '6e400010-b5a3-f393-e0a9-e50e24dcca9e';
const CHAR_DATA_UUID = '6e400011-b5a3-f393-e0a9-e50e24dcca9e';
const CHAR_CONTROL_UUID = '6e400012-b5a3-f393-e0a9-e50e24dcca9e';

const SERVICE_SECURITY_UUID = '6e400009-b5a3-f393-e0a9-e50e24dcca9e';
const CHAR_SECURITY_SEND = '6e400015-b5a3-f393-e0a9-e50e24dcca9e';
const CHAR_SECURITY_RECEIVE = '6e400014-b5a3-f393-e0a9-e50e24dcca9e';

/**
 * Callback from outside
 */
let callbackConnected = null;
let rejectConnected = null;
let callbackScan = null;
let rejectScan = null;

let callbackDisconnected = null;
let callbackUpdateChar = null;

const ScanAllDuration = Platform.select({ ios: 3, android: 3 });
const ScanDuration = Platform.select({ ios: 3, android: 3 });
const ScanMode = 2;

const hexToInt = hex => {
  return parseInt(`0x${hex}`);
};

export const getDeviceName = () => {
  return deviceName;
};

export const generateCommand = {
  askCurrentEffect: () => [0x01, 0x02, 0x03, 0x04],
  switchMode: effect => [0x01, 0x04, effect],
  switchEffect: (effect, mode) => [0x01, 0x03, mode, effect],
  switchColor: color => {
    const rgb = [
      hexToInt(color.slice(1, 3)),
      hexToInt(color.slice(3, 5)),
      hexToInt(color.slice(5, 7)),
    ];
    return [0x01, 0x05, ...rgb];
  },
  switchSpeed: speed => [0x01, 0x06, speed],

  security: (keys, deviceName) => {
    return [0x01, 0x01, ...keys];
  },
};

export const extractDataFromResponse = (data, dataType) => {
  switch (dataType) {
    case BLE_RESPONSE_DATA_TYPE.CURRENT_MODE:
      return {
        mode: data[2],
        effect: data[3],
        color: data.slice(4, 7),
        speed: data[7],
      };

    case BLE_RESPONSE_DATA_TYPE.SECURITY:
      const key = data.slice(2, 6);

      return key
    default:
      break;
  }
};

const getVersion = data => {
  const advertising = data.advertising;
  const raw = advertising.manufacturerData.bytes;
  try {
    let startPos;
    if (Platform.OS === 'ios') {
      startPos = 10;
    } else {
      startPos = 15;
    }
    const _deviceVersion = []; // array length = 4
    for (let i = startPos; i <= startPos + 2; i++) {
      if (raw[i] === undefined) return null;
      _deviceVersion.push(raw[i]);
    }
    return _deviceVersion;
  } catch (e) {
    return null;
  }
};

/**
 * Inside handlers
 */
const handleDiscoverPeripheral = async data => {
  console.log('handleDiscoverPeripheral', data);
  if (scanAction === 'scanToConnect') {
    if (deviceAddr) {
      return;
    }
    if (data.name !== deviceName && data.advertising.localName !== deviceName) {
      console.log("khong tim thay thiet bi :", deviceName);
      return;
    }

    const version = getVersion(data);
    if (version) {
      deviceVersion = version;
    } else {
      deviceVersion = [1, 0, 0];
    }

    // Found
    deviceAddr = data.id;
    await stopScan();
    connectedDeviceId = [];
    BleManager.connect(deviceAddr).catch(async e => {
      if (e.includes('Connection')) {
        console.log("in here find connection in E");
        await new Promise(resolve => setTimeout(resolve, 1000));
      }
      rejectConnected(e);
    });
  } else if (scanAction === 'scanOnly') {
    if (data.name === I8_BLE || data.name === WINNER_HAU) {
      if (!foundDeviceName.includes(data.name)) {
        foundDeviceName.push(data.name);
      }
    }
  }
};

const handleStopScan = () => {
  isScanning = false;
  if (scanAction === 'scanToConnect') {
    if (!deviceAddr) {
      rejectConnected(BLE_ERROR.NOT_FOUND);
    }
  } else {
    if (foundDeviceName.length === 0) {
      rejectScan(BLE_ERROR.NOT_FOUND);
    } else {
      callbackScan(foundDeviceName);
    }
  }
};
const handleDisconnectedPeripheral = () => {
  isConnected = false;
  callbackDisconnected && callbackDisconnected();
  rejectConnected && rejectConnected(BLE_ERROR.DISCONNECTED);
};
const handleUpdateValueForCharacteristic = data => {
  console.log('handleUpdateValueForCharacteristic', data.value);
  const { value } = data;
  // Callback when mode is changed by hardware
  if (value[0] === 0x02 && value[1] === 0x03) {
    console.log('CHANGE_MODE_FROM_HARDWARE');
    eventBus.fireEvent(EVENT_BUS.CHANGE_MODE_FROM_HARDWARE, data.value);
    return;
  }
  callbackUpdateChar && callbackUpdateChar(value);
  callbackUpdateChar = null;
};
const handleUpdateState = ({ state }) => {
  bleOn = state === 'on';
};
const handleConnectedPeripheral = async () => {
  if (isConnected) {
    return;
  }
  isConnected = true;

  try {
    await BleManager.retrieveServices(deviceAddr);
    console.log('handleConnectedPeripheral', ' pass retrieveServices');
    // Security
    await BleManager.startNotification(
      deviceAddr,
      SERVICE_SECURITY_UUID,
      CHAR_SECURITY_RECEIVE,
    );
    const response = await sendData({
      data: generateCommand.security([1, 2, 3, 4], deviceName),
      waitForResponse: true,
      service: SERVICE_SECURITY_UUID,
      char: CHAR_SECURITY_SEND,
    });
    const responseKey = extractDataFromResponse(
      response,
      BLE_RESPONSE_DATA_TYPE.SECURITY,
    );
    console.log('responseKey----------', responseKey);
    if (compareArray(responseKey, getDeviceName()) === false) {
      console.log("wrong key")
      rejectConnected(BLE_ERROR.BLE_SECURITY_FAIL);
      disconnect();
    }
    console.log('handleConnectedPeripheral', ' pass security');
    // Get current mode
    await BleManager.startNotification(
      deviceAddr,
      SERVICE_DATA_UUID,
      CHAR_DATA_UUID,
    );
    const currentModeData = await sendData({
      data: generateCommand.askCurrentEffect(),
      waitForResponse: true,
    });
    console.log('currentModeData', currentModeData);
    // Connect succeed
    callbackConnected &&
      callbackConnected({
        currentModeData,
        connectedDeviceId,
        deviceAddr,
        deviceVersion,
      });
    callbackConnected = null;
  } catch (e) {
    rejectConnected(e);
    disconnect();
  }
};

export async function init() {
  // Not init yet
  if (!didInit) {
    console.log('ble init');
    // Init module
    await BleManager.start({ showAlert: true });
    // Add event listeners
    didInit = bleManagerEmitter.addListener(
      'BleManagerDiscoverPeripheral',
      handleDiscoverPeripheral,
    );
    bleManagerEmitter.addListener(
      'BleManagerDidUpdateState',
      handleUpdateState,
    );
    bleManagerEmitter.addListener('BleManagerStopScan', handleStopScan);
    bleManagerEmitter.addListener(
      'BleManagerConnectPeripheral',
      handleConnectedPeripheral,
    );
    bleManagerEmitter.addListener(
      'BleManagerDisconnectPeripheral',
      handleDisconnectedPeripheral,
    );
    bleManagerEmitter.addListener(
      'BleManagerDidUpdateValueForCharacteristic',
      handleUpdateValueForCharacteristic,
    );
    BleManager.checkState();
    await new Promise(resolve => setTimeout(resolve, 175));
    // Permission check
    await requestPermission();
    // Enable BLE
    await BleManager.enableBluetooth();
  } else {
    console.warn('BLE ALREADY INIT');
  }
}

/**
 * Check BLE state before do any job
 * @param {*} job
 */
export async function doBleJob(job) {
  if (await requestPermission()) {
    if (bleOn) {
      return job();
    } else {
      // Alert("Vui lòng bật Bluetooth để sử dụng");
      throw new Error(BLE_ERROR.BLE_OFF);
    }
  }
  // Alert("Vui lòng bật cấp quyền Bluetooth để sử dụng");
}

export async function requestPermission() {
  if (Platform.OS === 'android' && Platform.Version >= 23) {
    const permitted = await PermissionsAndroid.check(
      PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
    );
    if (permitted) {
      console.log('Permission is OK');
      return true;
    }
    const requestResult = await PermissionsAndroid.requestPermission(
      PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
    );
    if (requestResult) {
      console.log('User accept');
      return true;
    }
    console.log('User refuse');
    throw new Error(BLE_ERROR.NO_PERMISSION);
  }
  return true;
}

async function stopScan() {
  if (isScanning) {
    await BleManager.stopScan();
    isScanning = false;
  } else {
    console.warn('BLE IS NOT SCANNING');
  }
}

export const scanOnly = () => {
  console.log('scanOnly');
  return new Promise(async (resolve, reject) => {
    if (!(await requestPermission())) {
      return reject(BLE_ERROR.NO_PERMISSION);
    }

    // Ble is off
    if (!bleOn) {
      return reject(BLE_ERROR.BLE_OFF);
    }

    if (!isScanning) {
      scanAction = 'scanOnly';
      foundDeviceName = [];
      BleManager.scan([], ScanAllDuration, false, { scanMode: ScanMode })
        .then(() => {
          console.log('Start scan');
          isScanning = true;
          rejectScan = reject;
          callbackScan = resolve;
        })
        .catch(e => {
          reject(e);
        });
    } else {
      console.warn('BLE ALREADY SCANNING');
      reject(BLE_ERROR.ALREADY_SCANNING);
    }
  });
};

export function connect(name: string) {
  console.log('connect to ', name);
  deviceName = name;
  return new Promise(async (resolve, reject) => {
    if (!(await requestPermission())) {
      return reject(BLE_ERROR.NO_PERMISSION);
    }

    // Ble is off
    if (!bleOn) {
      return reject(BLE_ERROR.BLE_OFF);
    }

    deviceAddr = null;
    if (!isConnected) {
      console.log("in here to do")
      // reset all other callbacks
      callbackUpdateChar = null;
      callbackDisconnected = null;
      // connect calback
      callbackConnected = resolve;
      rejectConnected = reject;
      if (!isScanning) {
        console.log("in here to do Scanning")
        scanAction = 'scanToConnect';
        BleManager.scan([], ScanDuration, false, { scanMode: ScanMode })
          .then(() => {
            console.log('Start scan1');
            isScanning = true;
          })
          .catch(e => {
            reject(e);
          });
      } else {
        console.warn('BLE ALREADY SCANNING');
        reject(BLE_ERROR.ALREADY_SCANNING);
      }
    } else {
      console.warn('BLE IS ALREADY CONNECTED');
      reject(BLE_ERROR.ALREADY_CONNECTED);
    }
  });
}

export function sendData({
  data,
  waitForResponse = false,
  service = SERVICE_DATA_UUID,
  char = CHAR_CONTROL_UUID,
}) {
  console.log('sendData', data, waitForResponse);
  if (waitForResponse) {
    return new Promise((resolve, reject) => {
      callbackUpdateChar = resolve;
      BleManager.write(deviceAddr, service, char, data).catch(e => reject(e));
    });
  } else {
    return BleManager.write(deviceAddr, service, char, data);
  }
}

export function addDisconnectCallback(callback) {
  callbackDisconnected = callback;
}

function disconnect() {
  isConnected = false;
  callbackDisconnected = null;
  return BleManager.disconnect(deviceAddr);
}

export async function destroyBle() {
  try {
    await stopScan();
    await disconnect();
  } catch (e) {
    console.warn('destroyBle', e);
  }
}

function compareArray(ar1, deviceName) {

  console.log('deviceName----', deviceName)
  let ar2;
  switch (deviceName) {
    case 'WINNER_HAU':
      ar2 = [0x01, 0x01, 0x01, 0x03];
      break
    case 'I8_BLE':
      ar2 = [0x04, 0x03, 0x02, 0x01];
      break
    case 'VAIRO_HAU':
      ar2 = [0x01, 0x01, 0x01, 0x05];
      break
    case 'VAIRO_DM':
      ar2 = [0x01, 0x01, 0x01, 0x04];
      break
    case 'GC':
      ar2 = [0x01, 0x01, 0x01, 0x06];
      break
    case 'WINNER_DM2':
      ar2 = [0x01, 0x01, 0x01, 0x02];
      break


  }


  console.log('deviceName----', ar1, ar2.length, ar1.length)


  if (ar1.length !== ar2.length) {
    return false;
  }
  for (let i = 0; i < ar1.length; i += 1) {
    if (ar1[i] !== ar2[i]) {
      return false;
    }
  }
  return true;
}

const h = [0x01, 0x02, 0x03, 0x04];
const l = [0x01, 0x10, 0x07, 0x09];

const key = [0x01, 0x02, 0x03, 0x04];

const generateReskey = key => {
  const reskey = key.map(
    (value, index) => parseInt((h[index] + l[index]) / 2) & value,
  );
  return reskey;
};
