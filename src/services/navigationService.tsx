import {NavigationActions, StackActions} from 'react-navigation';

let _navigator;
let currentScreen;

function setTopLevelNavigator(navigatorRef) {
  _navigator = navigatorRef;
}

function navigate(routeName, params) {
  _navigator.dispatch(
    NavigationActions.navigate({
      routeName,
      params,
    }),
  );
}

function goBack() {
  _navigator.dispatch(StackActions.pop());
}

function replace(routeName, params, action = {}) {
  _navigator.dispatch(StackActions.replace({routeName, params, action}));
}

// gets the current screen from navigation state
function getActiveRouteName(navigationState) {
  if (!navigationState) {
    return null;
  }
  const route = navigationState.routes[navigationState.index];
  // dive into nested navigators
  if (route.routes) {
    return getActiveRouteName(route);
  }
  return route.routeName;
}

function onNavigationStateChange(prevState, currentState) {
  const _currentScreen = getActiveRouteName(currentState);
  const prevScreen = getActiveRouteName(prevState);

  if (prevScreen !== _currentScreen) {
    // the line below uses the Google Analytics tracker
    // change the tracker here to use other Mobile analytics SDK.
    currentScreen = _currentScreen;

    console.group(
      '%cSCREEN CHANGE',
      'color:white;font-weight:bold;background:orange;padding:2px 6px',
      currentScreen,
    );
    console.log('Previous Screen\t\t', prevScreen);
    console.log('Current Screen\t\t', currentScreen);
    console.groupEnd();
  }
}

function getCurrentScreen() {
  return currentScreen;
}

// add other navigation functions that you need and export them

export default {
  replace,
  navigate,
  goBack,
  setTopLevelNavigator,
  onNavigationStateChange,
  getCurrentScreen,
};
