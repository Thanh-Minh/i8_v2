const colors = {
  mainLight: '#27A6E0',
  mainDark: '#3F3B3C',
  secondaryDark: '#797778',
};

export default colors;
