/**
 * @flow
 */

import React from 'react';
import { View, Text, Platform, Alert } from 'react-native';
import LottieView from 'lottie-react-native';
import Fs from 'react-native-fs';
import BleManager from 'react-native-ble-manager';

import DfuHandler from '../services/ble/DfuHandler';
import navigationService from '../services/navigationService';
import { I8_BLE } from '../constants';
import ScreenHeader from '../components/ScreenHeader';
import withSafeArea from '../components/WithSafeArea';
import colors from '../theme/colors';

const DfuFolder = 'dfu';

const ResourceFolder = `${Platform.select({
  ios: Fs.MainBundlePath,
  android: Fs.DocumentDirectoryPath,
})}/${DfuFolder}/`;
const DFUfile = ResourceFolder + 'winnerDM1_0_2.zip';
const RearFW = ResourceFolder + 'rear.zip';
const FrontFW = ResourceFolder + 'front.zip';

const cloneAssetToDocument = async (path: string) => {
  const assets = await Fs.readDirAssets(path);

  assets.forEach(async item => {
    if (item.isFile()) {
      // copy to document now
      console.log('clone file', item.path);
      await Fs.copyFileAssets(
        item.path,
        `${Fs.DocumentDirectoryPath}/${item.path}`,
      );
    } else if (item.isDirectory()) {
      // make a folder on document
      await Fs.mkdir(`${Fs.DocumentDirectoryPath}/${item.path}`);
      // dig deeper
      await cloneAssetToDocument(item.path);
    }
  });
};

export const cloneResourceFromAssetToDocument = async () => {
  if (Platform.OS === 'android') {
    const rootPath = `${Fs.DocumentDirectoryPath}/${DfuFolder}`;
    await Fs.mkdir(rootPath);

    await cloneAssetToDocument(DfuFolder);
  }
};

const MAX_TRY_DFU = 3;
const SCAN_DURATION = Platform.OS === 'android' ? 4 : 3; // s

type State = {
  isLoading: boolean;
  isDownloading?: boolean;
  isDFU?: boolean;
  progress: string;
  failure?: boolean;
  failureCode?: number;
  dfuState: string;
  countTry: number;
  /**
   *
   */
  deviceAddr: string;
  currentVersion: number[];
  lastestVersion: number[];
};

class DfuUpdateScreen extends React.Component<
  { navigation: Record<string, any> },
  State
  > {
  constructor(props: any) {
    super(props);
    this.state = {
      isLoading: true,
      isDownloading: false,
      isDFU: false,
      progress: '0%',
      failure: false,
      failureCode: 0,
      dfuState: '',
      deviceAddr: '',
      currentVersion: [],
      lastestVersion: [],
      countTry: 0,
    };
  }

  componentDidMount() {
    // Setup DFU callback
    DfuHandler.registerDFUProgressCallback(this.onDfuProgressChange);
    DfuHandler.registerDFUStateChangedCallback(this.onDfuStateChange);

    this.startDFU();
  }

  componentWillUnmount() {
    // Remove callback
    DfuHandler.unresgisterAllCallback();
  }

  onDownloadProgressChange = (percent: number) => {
    console.log(`Download progress: ${percent}%`);
    const str = `Đang chuẩn bị dữ liệu ${percent.toFixed(0)}%`;
    this.setState({ progress: str });
  };

  onDfuProgressChange = ({
    percent,
    currentPart,
    partsTotal,
    avgSpeed,
    speed,
  }: Record<string, any>) => {
    console.log(`DFU progress: ${percent}%`);
    const str = `Đang cài đặt ${percent}% (${currentPart}/${partsTotal})`;
    this.setState({ progress: str });
  };

  onDfuStateChange = ({ state }: Record<string, any>) => {
    console.log('DFU State:', state);
    this.setState({ dfuState: state });
    if (state === 'DFU_FAILED') {
      // FAILED
      if (this.state.countTry < MAX_TRY_DFU) {
        // RETRY
        this.setState(
          {
            countTry: this.state.countTry + 1,
            progress: `Tiến hành thử lại ${this.state.countTry +
              1}/${MAX_TRY_DFU}`,
          },
          this.startDFU,
        );
      } else {
        // ENOUGH RETRY
        this.onFailure();
      }
    } else if (state === 'DFU_COMPLETED') {
      Alert.alert(
        'Cập nhật thành công',
        'Vui lòng tắt Bluetooth và bật lại để tiếp tục sử dụng',
        [
          {
            text: 'Đồng ý',
            onPress: () => navigationService.navigate('SelectDevice'),
          },
        ],
        { cancelable: false },
      );
    } else if (state === 'CONNECTING') {
      this.setState({ progress: 'Đang kết nối thiết bị' });
    }
  };

  onFailure = () => {
    Alert.alert(
      'Cập nhật không thành công',
      '',
      [
        {
          text: 'Bỏ qua',
          onPress: () => navigationService.navigate('SelectDevice'),
        },
        {
          text: 'Thử lại',
          onPress: () => {
            this.setState(
              { countTry: 0, progress: 'Kiểm tra thiết bị' },
              this.startDFU,
            );
          },
        },
      ],
      { cancelable: false },
    );
  };

  handleBackPress = () => {
    Alert.alert(
      'Vui lòng đợi cập nhật hoàn tất',
      'Huỷ quá trình có thể khiến thiết bị ngừng hoạt động',
      [{ text: 'Đồng ý' }],
      {
        cancelable: false,
      },
    );
    return true;
  };

  startDFU = async () => {
    const deviceAddr = this.props.navigation.getParam('deviceAddr');
    const deviceName = this.props.navigation.getParam('deviceName');
    this.setState({ progress: 'Kiểm tra thiết bị' });
    // Scan trước khi bắt đầu -> Tránh lỗi, để BLE stack nhận được thiết bị
    BleManager.scan([], SCAN_DURATION, true, { scanMode: 1 }).then(() => {
      // Success code
      console.log('Scan for DFU started');
    });
    await new Promise(r => {
      setTimeout(r, (SCAN_DURATION + 0.5) * 1000);
    });
    //const file = deviceName === I8_BLE ? FrontFW : RearFW;
    const file = FrontFW;
    DfuHandler.startDFU(deviceAddr, file, deviceName);
  };

  renderBody = () => {
    if (this.state.failure) {
      return null;
    }
    return (
      <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
        {/* Animated View */}
        <LottieView
          style={{
            width: 250,
            height: 250,
            marginBottom: 10,
          }}
          source={require('../assets/animationJson/dfu.json')}
          loop
          autoPlay
        />
        <Text style={{ fontSize: 20, color: 'white' }}>
          {this.state.progress}
        </Text>

        <Text
          style={{
            marginTop: 10,
            color: 'gray',
            textAlign: 'center',
          }}>
          Giữ khoảng cách gần với thiết bị của bạn.
          {'\n'} Quá trình có thể diễn ra từ 3 đến 5 phút
        </Text>
        <Text
          style={{
            color: '#ff000090',
            marginTop: 10,
            textAlign: 'center',
          }}>
          Không tắt ứng dụng hoặc khoá điện
          {'\n'}
          khi cập nhật
        </Text>
      </View>
    );
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        <ScreenHeader title="Cập nhật phần cứng" rightButton={false} />
        {this.renderBody()}
      </View>
    );
  }
}

export default withSafeArea({
  style: { backgroundColor: colors.mainDark },
  topColor: colors.secondaryDark,
  barStyle: 'light-content',
})(DfuUpdateScreen);
