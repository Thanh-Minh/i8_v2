import React from 'react';
import {ImageBackground, Image, View} from 'react-native';
import WithConfigScreen from '../components/WithConfigScreen';
import WithBLeConfigScreen from '../components/WithBLeConfigScreen';
import Images from '../assets/Images';
import ble from '../services/ble';
import {LED_MODE, BLE_RESPONSE_DATA_TYPE, VAIRO_DM} from '../constants';
import {showScreenLoading} from '../components/ScreenLoading';

const tabBarHeight = 30;
const tabBarWidth = 23;
const SCREEN_TITLE = 'HIỆU ỨNG KHỞI ĐỘNG';
const TAB_TITLE = 'START';
let deviceName: any;
const EFFECT_LIST1 = [
  {effect: 1, name: 'DEMI 1', speed: true, color: true},
  {effect: 2, name: 'DEMI 2', speed: true, color: true},
  {effect: 3, name: 'DEMI 3', speed: true, color: true},
];

const EFFECT_LIST = [
  {effect: 1, name: '1', speed: true, color: true},
  {effect: 2, name: '2', speed: true, color: true},
  {effect: 3, name: '3', speed: true, color: true},
  {effect: 4, name: '4', speed: true, color: true},
  {effect: 5, name: '5', speed: true, color: true},
  // {effect: 6, name: '6', speed: true, color: true},
  // {effect: 7, name: '7', speed: true, color: true},
  // {effect: 8, name: '8', speed: true, color: true},
  // {effect: 9, name: '9', speed: true, color: true},
  // {effect: 10, name: 'A', speed: true, color: true},
];

class BLeStartScreen extends React.PureComponent {
  static navigationOptions = () => {
    return {
      title: TAB_TITLE,

      tabBarIcon: ({focused, tintColor}) => (
        <ImageBackground
          style={{width: tabBarWidth, height: tabBarHeight}}
          resizeMode="contain"
          imageStyle={{tintColor: focused ? tintColor : 'white'}}
          source={Images.demi}
        />
      ),
    };
  };

  onWillFocus = async () => {
    try {
      await showScreenLoading(true);
      const configData = ble.generateCommand.switchMode(LED_MODE.DEMI);
      await ble.sendData({data: configData});
      const feedbackData = await ble.sendData({
        data: ble.generateCommand.askCurrentEffect(),
        waitForResponse: true,
      });
      await showScreenLoading(false);
      return ble.extractDataFromResponse(
        feedbackData,
        BLE_RESPONSE_DATA_TYPE.CURRENT_MODE,
      );
    } catch (e) {
      console.warn('onWillFocus', e);
    }
  };

  onChangeEffecList() {
    switch (deviceName) {
      case 'VAIRO_DM':
      case 'I8_BLE':
        return EFFECT_LIST1;
      case 'GC':
      case 'WINNER_HAU':
      case 'VAIRO_HAU':
        return EFFECT_LIST;
      default:
        return []
    }
  }

  render() {
    const {navigation} = this.props;
    deviceName = navigation.getParam('device');
    return (
      <WithBLeConfigScreen
        showModeList={false}
        speedConfigurable={false}
        maxColumns={2}
        mode={LED_MODE.DEMI}
        title={SCREEN_TITLE}
        onWillFocus={this.onWillFocus}
        effectList={EFFECT_LIST}
      />
    );
  }
}

export default BLeStartScreen;
