import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  screenBodyContainer: {
    flex: 1,
    paddingHorizontal: 20,
  },
  screenTitle: {
    alignSelf: 'center',
    padding: 10,
    fontSize: 18,
  },
  colorPicker: {
    paddingTop: 10,
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  circlePicked: {
    borderRadius: 50,
    width: 50,
    height: 50,
    backgroundColor: '#EEEEEE',
    borderWidth: 3,
    borderColor: '#EEEEEE',
    elevation: 3,
    shadowColor: 'rgb(46, 48, 58)',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.8,
    shadowRadius: 2,
  },
  title: {
    paddingBottom: 10,
  },
  configWrapper: {
    paddingTop: 15,
  },
});
