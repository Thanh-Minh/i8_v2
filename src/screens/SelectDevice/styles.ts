import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  scene: {flex: 1},
  body: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnDevice: {marginTop: 36},
  deviceImage: {
    width: 227,
    height: 153,
    resizeMode: 'contain',
  },
});
