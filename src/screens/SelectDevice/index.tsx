/**
 * @flow
 */
import React, {PureComponent} from 'react';
import {View, ScrollView, TouchableOpacity, Image, Text} from 'react-native';

// import ActiveDeviceAnimate from "../components/ActiveDeviceAnimate";
import withSafeArea from '../../components/WithSafeArea';
import {
  I8_BLE,
  WINNER_HAU,
  GC,
  VAIRO_HAU,
  STOP_F1_BLE,
  TURN,
} from '../../constants';
import colors from '../../theme/colors';
import ScreenHeader from '../../components/ScreenHeader';
import styles from './styles';
import Images from '../../assets/Images';
import navigationService from '../../services/navigationService';
import AppVersion from '../../components/AppVersion';

type Props = {};
type States = {};

class SelectDevice extends PureComponent<Props, States> {
  constructor(props: Record<string, any>) {
    super(props);
  }

  render() {
    return (
      <View style={styles.scene}>
        <ScreenHeader title="LỰA CHỌN THIẾT BỊ" rightButton={false} />
        <ScrollView contentContainerStyle={styles.body}>
          <TouchableOpacity
            style={{}}
            onPress={() => {
              navigationService.navigate('ScanningScreen', {device: GC});
            }}>
            <Image style={styles.deviceImage} source={Images.typeDeviceRGB} />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.btnDevice}
            onPress={() => {
              navigationService.navigate('SelectType', {
                typeDevice: 1,
              });
            }}>
            <Image
              style={styles.deviceImage}
              source={Images.typeDeviceWinner}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.btnDevice}
            onPress={() => {
              navigationService.navigate('SelectType', {
                typeDevice: 2,
              });
            }}>
            <Image style={styles.deviceImage} source={Images.typeDeviceVairo} />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.btnDevice}
            onPress={() => {
              // TODO thay doi device
              navigationService.navigate('ScanningScreen', {
                device: VAIRO_HAU,
                specificDevice: 'F1_BLE',
              });
            }}>
            {/* todo thay doi anh */}
            <Image style={styles.deviceImage} source={Images.typeDeviceVairo} />
            <Text>STOP F1 BLE</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.btnDevice}
            onPress={() => {
              // TODO thay doi device
              navigationService.navigate('ScanningScreen', {device: VAIRO_HAU});
            }}>
            {/* todo thay doi anh */}
            <Image style={styles.deviceImage} source={Images.typeDeviceVairo} />
            <Text>XIN NHAN</Text>
          </TouchableOpacity>
        </ScrollView>
        <AppVersion />
      </View>
    );
  }
}

export default withSafeArea({
  style: {backgroundColor: colors.mainDark},
  topColor: colors.secondaryDark,
  barStyle: 'light-content',
})(SelectDevice);
