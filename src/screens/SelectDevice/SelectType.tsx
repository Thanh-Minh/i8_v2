/**
 * @flow
 */
import React, { PureComponent } from 'react';
import {
  View,
  TouchableOpacity,
  Image,
  Text,
} from 'react-native';

import ActiveDeviceAnimate from "../../components/ActiveDeviceAnimate";
import withSafeArea from '../../components/WithSafeArea';
import { I8_BLE, WINNER_HAU, VAIRO_DM, VAIRO_HAU, WINNER_DM2 } from '../../constants';
import colors from '../../theme/colors';
import ScreenHeader from '../../components/ScreenHeader';
import styles from './styles';
import Images from '../../assets/Images';
import navigationService from '../../services/navigationService';
import AppVersion from '../../components/AppVersion';

type Props = {};
type States = {};


class SelectType extends React.Component {
  render() {
    const { navigation } = this.props;
    if ((navigation.getParam('typeDevice') === 1)) {
      return (
        <View style={styles.scene}>
          <ScreenHeader title="WINNER X" rightButton={true} navigation={navigation} />
          <View style={styles.body}>
            <TouchableOpacity
              style={styles.btnDevice}
              onPress={() => {
                navigationService.navigate('ScanningScreen', { device: I8_BLE });
              }}>
              <Image style={styles.deviceImage} source={Images.demiWinnerV1} />
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.btnDevice}
              onPress={() => {
                navigationService.navigate('ScanningScreen', { device: WINNER_DM2 });
              }}>
              <Image style={styles.deviceImage} source={Images.demiWinnerV2} />
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.btnDevice}
              onPress={() => {
                navigationService.navigate('ScanningScreen', { device: WINNER_HAU });
              }}>
              <Image style={styles.deviceImage} source={Images.rearWinner} />
            </TouchableOpacity>
          </View>
          <AppVersion />
        </View>
      );
    }else {
      return (
        <View style={styles.scene}>
          <ScreenHeader title="VAIRO" rightButton={true} />
          <View style={styles.body}>
            <TouchableOpacity
              style={{}}
              onPress={() => {
                navigationService.navigate('ScanningScreen', { device: VAIRO_DM });
              }}>
              <Image style={styles.deviceImage} source={Images.demiVairo} />
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.btnDevice}
              onPress={() => {
                navigationService.navigate('ScanningScreen', { device: VAIRO_HAU });
              }}>
              <Image style={styles.deviceImage} source={Images.rearVairo} />
            </TouchableOpacity>
          </View>
          <AppVersion />
        </View>
      );
    }
  }
}



export default withSafeArea({
  style: { backgroundColor: colors.mainDark },
  topColor: colors.secondaryDark,
  barStyle: 'light-content',
})(SelectType);