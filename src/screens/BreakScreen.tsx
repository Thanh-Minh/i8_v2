import React from 'react';
import {ImageBackground} from 'react-native';

import WithConfigScreen from '../components/WithConfigScreen';
import Images from '../assets/Images';
import {showScreenLoading} from '../components/ScreenLoading';
import ble from '../services/ble';
import {LED_MODE, BLE_RESPONSE_DATA_TYPE} from '../constants';

const tabBarWidth = 36;
const tabBarHeight = 30;
const SCREEN_TITLE = 'CÀI ĐẶT PHANH';
const TAB_TITLE = 'PHANH';

const EFFECT_LIST = [
  {effect: 1, name: '1', speed: true, color: true},
  {effect: 2, name: '2', speed: true, color: true},
  {effect: 3, name: '3', speed: true, color: true},
  {effect: 4, name: '4', speed: true, color: true},
  {effect: 5, name: '5', speed: true, color: true},
];

class BreakScreen extends React.PureComponent {
  static navigationOptions = () => {
    return {
      title: TAB_TITLE,
      tabBarIcon: ({focused, tintColor}) => (
        <ImageBackground
          style={{width: tabBarWidth, height: tabBarHeight}}
          resizeMode="contain"
          imageStyle={{tintColor: focused ? tintColor : null}}
          source={Images.phanh}
        />
      ),
    };
  };

  onWillFocus = async () => {
    try {
      await showScreenLoading(true);
      const configData = ble.generateCommand.switchMode(LED_MODE.BREAK);
      await ble.sendData({data: configData});
      const feedbackData = await ble.sendData({
        data: ble.generateCommand.askCurrentEffect(),
        waitForResponse: true,
      });
      await showScreenLoading(false);
      return ble.extractDataFromResponse(
        feedbackData,
        BLE_RESPONSE_DATA_TYPE.CURRENT_MODE,
      );
    } catch (e) {
      console.warn('onWillFocus', e);
    }
  };

  render() {
    return (
      <WithConfigScreen
        maxColumns={3}
        mode={LED_MODE.BREAK}
        title={SCREEN_TITLE}
        onWillFocus={this.onWillFocus}
        effectList={EFFECT_LIST}
      />
    );
  }
}

export default BreakScreen;
