import React from 'react';
import { ImageBackground, NativeAppEventEmitter } from 'react-native';

import WithConfigScreen from '../components/WithConfigScreen';
import Images from '../assets/Images';
import ble from '../services/ble';
import { LED_MODE, BLE_RESPONSE_DATA_TYPE, VAIRO_DM } from '../constants';
import { showScreenLoading } from '../components/ScreenLoading';

const tabBarHeight = 30;
const tabBarWidth = 23;
const SCREEN_TITLE = 'CÀI ĐẶT DEMI';
const TAB_TITLE = 'DEMI';
let deviceName: any;
const EFFECT_LIST = [
  { effect: 1, name: 'DEMI 1', speed: true, color: true },
  { effect: 2, name: 'DEMI 2', speed: true, color: true },
];
const EFFECT_LIST1 = [
  { effect: 1, name: 'DEMI 1', speed: true, color: true },
  { effect: 2, name: 'DEMI 2', speed: true, color: true },
  { effect: 3, name: 'DEMI 3', speed: true, color: true },
];



class DemiScreen extends React.PureComponent {
  static navigationOptions = () => {
    return {
      title: TAB_TITLE,

      tabBarIcon: ({ focused, tintColor }) => (
        <ImageBackground
          style={{ width: tabBarWidth, height: tabBarHeight }}
          resizeMode="contain"
          imageStyle={{ tintColor: focused ? tintColor : 'white' }}
          source={Images.demi}
        />
      ),
    };
  };
  onWillFocus = async () => {
    try {
      await showScreenLoading(true);
      const configData = ble.generateCommand.switchMode(LED_MODE.DEMI);
      await ble.sendData({ data: configData });
      const feedbackData = await ble.sendData({
        data: ble.generateCommand.askCurrentEffect(),
        waitForResponse: true,
      });
      await showScreenLoading(false);
      return ble.extractDataFromResponse(
        feedbackData,
        BLE_RESPONSE_DATA_TYPE.CURRENT_MODE,
      );
    } catch (e) {
      console.warn('onWillFocus', e);
    }
  };

  onChangeEffecList() {
    let effectList: any[];
    console.log('aloa alo', deviceName)
    switch (deviceName) {
      case 'VAIRO_DM':
      case 'I8_BLE':

        effectList = EFFECT_LIST1;
        break
      case 'GC':
      case 'WINNER_HAU':
      case 'VAIRO_HAU':
        effectList = EFFECT_LIST;
        break;
      default:
        break

    }
    return effectList;
  }

  render() {
    const { navigation } = this.props;


    deviceName = navigation.getParam('device')
    console.log('deviceName : -----', deviceName)
    return (
      <WithConfigScreen
        showModeList={false}
        speedConfigurable={false}
        maxColumns={2}
        mode={LED_MODE.DEMI}
        title={SCREEN_TITLE}
        onWillFocus={this.onWillFocus}
        //effectList={(effectListDM: any) => { this.onChangeEffecList(effectListDM) }}  // TODO:  
        effectList={(deviceName === ('VAIRO_DM') || deviceName === ('WINNER_DM2')) ? EFFECT_LIST1 : EFFECT_LIST}

      />
    );
  }
}

export default DemiScreen;
