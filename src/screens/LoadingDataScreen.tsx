/**
 * @flow
 */
import React from 'react';
import {View, ImageBackground, AsyncStorage, StatusBar} from 'react-native';
import LottieView from 'lottie-react-native';

import Images from '../assets/Images';
import ble from '../services/ble';
import navigationService from '../services/navigationService';
import {ASYNC_STORAGE_ITEMS} from '../constants';
import colors from '../theme/colors';
import AppVersion from '../components/AppVersion';

type Props = {};
type States = {};

class LoadingScreen extends React.PureComponent<Props, States> {
  async componentDidMount() {
    await ble.requestPermission();
    const isAuth = true;
    if (isAuth) {
      await new Promise(r => setTimeout(r, 1500));
      navigationService.navigate('SelectDevice');
    } else {
      navigationService.navigate('Auth');
    }
  }

  render() {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: colors.mainDark,
        }}>
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <StatusBar
            backgroundColor={colors.mainDark}
            barStyle="light-content"
          />
          <ImageBackground
            source={Images.logoTransparent}
            style={{height: 250, width: 250, marginBottom: 10}}
            resizeMode="contain"
          />
          <LottieView
            style={{height: 50, width: 200}}
            source={require('../assets/animationJson/loading.json')}
            autoPlay
            loop
          />
        </View>
        <AppVersion />
      </View>
    );
  }
}

export default LoadingScreen;
