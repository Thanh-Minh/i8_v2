/**
 * @flow
 */
import React, {PureComponent} from 'react';
import {
  View,
  StyleSheet,
  Text,
  AsyncStorage,
  ToastAndroid,
  BackHandler,
  Alert,
} from 'react-native';
import LottieView from 'lottie-react-native';

// import ActiveDeviceAnimate from "../components/ActiveDeviceAnimate";
import withSafeArea from '../components/WithSafeArea';
import ble from '../services/ble';
import Popup, {setRef, responsePopup} from '../components/Popup';
import {
  BLE_RESPONSE_DATA_TYPE,
  LED_MODE,
  BLE_ERROR,
  I8_BLE,
  WINNER_HAU,
} from '../constants';
import BackHandlerHelper from '../services/BackHandlerHelper';
import PopupSelectDevice from '../components/PopupSelectDevice';
import AppVersion from '../components/AppVersion';
import colors from '../theme/colors';
import DfuHandler from '../services/ble/DfuHandler';
import navigationService from '../services/navigationService';

type Props = {};
type States = {};

class ScanningScreen extends PureComponent<Props, States> {
  constructor(props: Record<string, any>) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    setTimeout(this.startConnect, 500);
  }

  startConnect = async () => {
    const {navigation} = this.props;
    // Load default device ID
    const deviceId = await getDefaultDevice();
    try {
      const deviceName = this.props.navigation.getParam('device');
      const {
        currentModeData,
        connectedDeviceId,
        deviceVersion,
        deviceAddr,
      } = await ble.connect(deviceName);

      // console.log("currmModeData :", currentModeData);
      // console.log("connectedDeviceId :", connectedDeviceId);
      // console.log("deviceVersion :", deviceVersion);
      // console.log("deviceAddr :", deviceAddr);
      // Is need to update?
      if (DfuHandler.checkUpdateForHW(deviceName, deviceVersion) > 0) {
        const agree = await new Promise(r => {
          Alert.alert(
            'Có bản cập nhật mới cho thiết bị',
            'Cập nhật để có trải nghiệm tốt hơn',
            [
              {
                text: 'Đồng ý',
                onPress: () => {
                  navigationService.navigate('Dfu', {deviceName, deviceAddr});
                  r(true);
                },
              },
              {
                text: 'Bỏ qua',
                onPress: () => {
                  r(false);
                },
                style: 'destructive',
              },
            ],
            {cancelable: false},
          );
        });
        if (agree) {
          return;
        }
      }

      // connect okay => navigate to AppTab
      const {mode, effect, color, speed} = ble.extractDataFromResponse(
        currentModeData,
        BLE_RESPONSE_DATA_TYPE.CURRENT_MODE,
      );
      const specificDevice = navigation.getParam('specificDevice', '');
      switch (mode) {
        case LED_MODE.DEMI:
          if (specificDevice === 'F1_BLE') {
            navigation.navigate('AppBLe');
          } else {
            navigation.navigate('DemiScreen', {
              device: ble.getDeviceName(),
              currentModeData: {effect, color, speed},
            });
          }
          break;
        case LED_MODE.EFFECT:
          if (specificDevice === 'F1_BLE') {
            navigation.navigate('AppBLe');
          } else {
            navigation.navigate('EffectScreen', {
              currentModeData: {effect, color, speed},
            });
          }
          break;
        default:
          if (specificDevice === 'F1_BLE') {
            navigation.navigate('AppBLe');
          } else {
            navigation.navigate('App');
          }
          break;
      }
      // Not have default device yet -> save
      if (!deviceId) {
        saveDefaultDevice(connectedDeviceId);
      }
    } catch (e) {
      console.log('e', e);
      let subtitle;
      if (e.message === 'cancel') {
        subtitle = 'Người dùng từ chối kết nối';
      } else {
        subtitle = translateSubtitle(e);
      }
      // Show popup fail
      responsePopup({
        title: 'Kết nối không thành công',
        subtitle: subtitle,
        touchOutsideToDismiss: false,
        retryCb: this.startConnect,
      });
    }
  };

  onBackPress = () => {
    if (!this.backPressed) {
      this.backPressed = true;
      ToastAndroid.show(
        'Ấn back một lần nữa để thoát ứng dụng.',
        ToastAndroid.SHORT,
      );
      setTimeout(() => {
        this.backPressed = false;
      }, 2000);
    } else {
      BackHandler.exitApp();
    }
  };

  render() {
    return (
      <View
        style={{
          flex: 1,
        }}>
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          {/* Hình minh hoạ */}
          {/* <ActiveDeviceAnimate size={265} /> */}
          <LottieView
            style={{width: 250, height: 250, marginBottom: 10}}
            source={require('../assets/animationJson/search.json')}
            loop
            autoPlay
          />
          {/* Header */}
          <Text style={styles.headerText}>Đang kết nối với thiết bị</Text>
        </View>
        <AppVersion />
        <Popup ref={_ref => setRef(_ref)} />
        <PopupSelectDevice ref={_ref => (this.popupSelectDevice = _ref)} />
        <BackHandlerHelper
          screenName="ScanningScreen"
          onBackPress={this.onBackPress}
        />
      </View>
    );
  }
}

const translateSubtitle = error => {
  switch (error) {
    case BLE_ERROR.BLE_OFF:
      return 'Vui lòng bật Bluetooth';

    case BLE_ERROR.DISCONNECTED:
      return 'Kết nối gián đoạn';

    case BLE_ERROR.NOT_FOUND:
      return 'Không tìm thấy thiết bị';

    case BLE_ERROR.NO_PERMISSION:
      return 'Ứng dụng không có quyền để sử dụng Bluetooth';

    case BLE_ERROR.BLE_SECURITY_FAIL:
      return 'Thiết bị không tương thích';

    default:
      return error;
  }
};

const getDefaultDevice = async () => {
  return null;
  try {
    const deviceIdStr = await AsyncStorage.getItem('deviceId');
    const deviceId = JSON.parse(deviceIdStr);
    return deviceId;
  } catch (e) {
    return null;
  }
};

const saveDefaultDevice = async deviceId => {
  try {
    const deviceIdStr = JSON.stringify(deviceId);
    await AsyncStorage.setItem('deviceId', deviceIdStr);
  } catch (e) {
    return null;
  }
};

const styles = StyleSheet.create({
  headerText: {
    alignSelf: 'center',
    padding: 10,
    fontSize: 18,
    color: 'white',
  },
  progressText: {
    marginTop: 10,
    fontSize: 18,
    alignSelf: 'center',
  },
  continueBut: {
    padding: 10,
    marginBottom: 15,
    alignSelf: 'center',
  },
  continueText: {},
});

export default withSafeArea({
  topColor: colors.mainDark,
  style: {backgroundColor: colors.mainDark},
  barStyle: 'light-content',
})(ScanningScreen);
