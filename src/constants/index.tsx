export const BLE_ERROR = {
  BLE_OFF: 'BLE_OFF',
  NO_PERMISSION: 'NO_PERMISSION',
  NOT_FOUND: 'NOT_FOUND',
  ALREADY_SCANNING: 'ALREADY_SCANNING',
  ALREADY_CONNECTED: 'ALREADY_CONNECTED',
  BLE_SECURITY_FAIL: 'BLE_SECURITY_FAIL',
  DISCONNECTED: 'DISCONNECTED',
};

export const BLE_RESPONSE_DATA_TYPE = {
  CURRENT_MODE: 'CURRENT_EFFECT',
  SECURITY: 'SECURITY',
};

export const LED_MODE = {
  DEMI: 1,
  EFFECT: 2,
  TURN_INDICATOR: 3,
  BREAK: 4,
  HAZARD: 5,
};
export const WINNER_DM2 = 'WINNER_DM2';
export const I8_BLE = 'I8_BLE';
export const WINNER_HAU = 'WINNER_HAU';
export const VAIRO_DM = 'VAIRO_DM';
export const VAIRO_HAU = 'VAIRO_HAU';
export const GC = 'GC'
export const STOP_F1_BLE = 'STOP_F1_BLE'
export const TURN = 'TURN'

export const EVENT_BUS = {
  CHANGE_MODE_FROM_HARDWARE: 'CHANGE_MODE_FROM_HARDWARE',
};

export const ASYNC_STORAGE_ITEMS = {
  IS_AUTH: 'IS_AUTH',
};

export const WEBSITE = 'http://ledxe.vn';
